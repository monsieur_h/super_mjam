LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE := cocos2dcpp_shared

LOCAL_MODULE_FILENAME := libcocos2dcpp

LOCAL_SRC_FILES := hellocpp/main.cpp \
                   ../../Classes/AppDelegate.cpp \
                   ../../Classes/animationlayer.cpp \
                   ../../Classes/collisionmanager.cpp \
                   ../../Classes/draftline.cpp \
                   ../../Classes/gameoverscreen.cpp \
                   ../../Classes/gamescreen.cpp \
                   ../../Classes/hudlayer.cpp \
                   ../../Classes/level.cpp \
                   ../../Classes/menuscreen.cpp \
                   ../../Classes/obstacle.cpp \
                   ../../Classes/physicalgameobject.cpp \
                   ../../Classes/player.cpp \
                   ../../Classes/playerplatform.cpp \
                   ../../Classes/sharedconfig.cpp \
                   ../../Classes/splashscreen.cpp \
                   ../../Classes/universescreen.cpp \
                   ../../Classes/world.cpp \
                   ../../Classes/worldmap.cpp \
                   ../../Classes/ext/B2DebugDraw/B2DebugDrawLayer.cpp \
                   ../../Classes/ext/B2DebugDraw/GLES-Render.cpp \
                   ../../Classes/ext/pugixml/pugixml.cpp

LOCAL_C_INCLUDES := $(LOCAL_PATH)/../../Classes

LOCAL_WHOLE_STATIC_LIBRARIES += cocos2dx_static
LOCAL_WHOLE_STATIC_LIBRARIES += cocosdenshion_static
LOCAL_WHOLE_STATIC_LIBRARIES += box2d_static
LOCAL_WHOLE_STATIC_LIBRARIES += chipmunk_static
LOCAL_WHOLE_STATIC_LIBRARIES += cocos_extension_static

include $(BUILD_SHARED_LIBRARY)

$(call import-module,cocos2dx)
$(call import-module,cocos2dx/platform/third_party/android/prebuilt/libcurl)
$(call import-module,CocosDenshion/android)
$(call import-module,extensions)
$(call import-module,external/Box2D)
$(call import-module,external/chipmunk)
