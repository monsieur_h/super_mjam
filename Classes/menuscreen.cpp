#include "menuscreen.h"
#include "sharedconfig.h"
#include <string>
#include "dirent.h"
USING_NS_CC;

bool MenuScreen::init()
{
    CCLayerColor* menuLayer = CCLayerColor::create(BLACK);
    this->addChild(menuLayer);
    _mainMenu = CCMenu::create();
    this->addChild(_mainMenu);

    const float FONT_SIZE = SharedConfig::get()->menuItemFontSize;
    const float MARGIN = SharedConfig::get()->menuItemMargin;

    std::vector<CCMenuItemFont*> items;


    //New Game item
    items.push_back(CCMenuItemFont::create(
        "New Game",
        this,
        menu_selector(MenuScreen::onNewGame)));

    // Levels
    std::vector<string> levelList = Level::getAvailableLevelNames();
    int i=0;
    for(i=0;i<levelList.size();i++)
    {
        CCLog("Found level", levelList[i].c_str());
                CCMenuItemFont* pLevel = CCMenuItemFont::create(
                    (levelList[i]).c_str(),
                    this,
                    menu_selector(MenuScreen::onLoadLevel));
                items.push_back(pLevel);
                pLevel->setTag(i);
    }

    //Options item
    items.push_back(CCMenuItemFont::create(
        "Options",
        this,
        menu_selector(MenuScreen::onOptions)));

    //Exit
    items.push_back(CCMenuItemFont::create(
        "Exit",
        this,
        menu_selector(MenuScreen::onExit)));

    float yPos= FONT_SIZE * (items.size()-1) + MARGIN * (items.size()-1);
    yPos /= 2;

    for(unsigned int i=0; i<items.size(); i++)
    {
		items[i]->setFontSizeObj(FONT_SIZE);
		items[i]->setFontNameObj(FONT_NAME);
		items[i]->setColor(WHITE_3);
        items[i]->setPositionY(yPos);
        _mainMenu->addChild(items[i]);
		yPos -= items[i]->fontSizeObj() + MARGIN;
    }
    return true;
}

void MenuScreen::onNewGame(CCObject *pSender)
{
    std::vector<string> list = Level::getAvailableLevelNames();
    GameScreen::setLevelFile(list[0]);
    GameScreen* gamescene = GameScreen::create();
    CCDirector::sharedDirector()->replaceScene(CCTransitionFade::create(1.0f, gamescene));
}

void MenuScreen::onLoadLevel(CCObject *pSender)
{
    std::vector<string> levelList = Level::getAvailableLevelNames();
    int clickedIndex = ((CCMenuItemFont*) pSender)->getTag();
    string levelName = levelList[clickedIndex];
    GameScreen::setLevelFile(levelName);
    GameScreen* gamescene = GameScreen::create();
    CCDirector::sharedDirector()->replaceScene(CCTransitionFade::create(1.0f, gamescene));
}

void MenuScreen::onOptions(CCObject *pSender)
{
    CCLog("Options");
}

void MenuScreen::onExit(CCObject *pSender)
{
    CCDirector::sharedDirector()->end();
    exit(0);
}
