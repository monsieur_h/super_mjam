#include "gameoverscreen.h"
#include "sharedconfig.h"
#include "worldmap.h"
#include "world.h"
#include "SimpleAudioEngine.h"

USING_NS_CC;

bool GameoverScreen::win = false;

bool GameoverScreen::init()
{
    if (!GameoverScreen::win) CocosDenshion::SimpleAudioEngine::sharedEngine()->playBackgroundMusic("sound/lose.wav", true);
	
    const float FONT_SIZE = SharedConfig::get()->menuItemFontSize;
    const float MARGIN = SharedConfig::get()->menuItemMargin;
    const float TITLE_SIZE = SharedConfig::get()->menuTitleFontSize;

	CCLayerColor* menuLayer = CCLayerColor::create(BLACK);
    this->addChild(menuLayer);

    _mainMenu = CCMenu::create();
    this->addChild(_mainMenu);

    std::vector<CCMenuItemFont*> items;

    // Title
    string titleMessage;
    if(GameoverScreen::win)
    {
        if(SharedConfig::get()->isLastLevel())
        {
            World* w = SharedConfig::get()->getLastWorld();
            titleMessage = "You finished the world " + w->getName();
        }
        else
        {
            titleMessage = "You won";
        }
    }
    else
    {
        titleMessage = "You lost";
    }
    CCMenuItemFont* pTitle = CCMenuItemFont::create(titleMessage.c_str());
    pTitle->setEnabled(false);
    items.push_back(pTitle);

    // Next level
    if(!GameScreen::isLastLevel() && GameoverScreen::win){
        items.push_back(CCMenuItemFont::create(
            "Next level",
            this,
            menu_selector(GameoverScreen::onNextLevel)));
    }

    // Play again
    items.push_back(CCMenuItemFont::create(
        "Play again",
        this,
        menu_selector(GameoverScreen::onPlayAgain)));

    // Menu
    items.push_back(CCMenuItemFont::create(
        "World map",
        this,
        menu_selector(GameoverScreen::onMenu)));

    float menuHeight = TITLE_SIZE + FONT_SIZE * (items.size()-2) + MARGIN * (items.size()-1);
    float yPos = menuHeight / 2;

    for(int i=0; i<items.size(); i++)
    {
		if(i==0){
			items[i]->setFontSizeObj(TITLE_SIZE);
		}
		else{
			items[i]->setFontSizeObj(FONT_SIZE);
		}
		items[i]->setFontNameObj(FONT_NAME);
        items[i]->setColor(WHITE_3);
        items[i]->setPositionY(yPos);
        _mainMenu->addChild(items[i]);
		yPos -= items[i]->fontSizeObj() + MARGIN;
    }

    SharedConfig::get()->onLevelOver(GameoverScreen::win);

    return true;
}

void GameoverScreen::onPlayAgain(CCObject *pSender)
{
    GameScreen* gamescene = GameScreen::create();
    CCDirector::sharedDirector()->replaceScene(CCTransitionFade::create(1.0f, gamescene));
    if (!GameoverScreen::win) CocosDenshion::SimpleAudioEngine::sharedEngine()->playBackgroundMusic("sound/background.wav", true);
}

void GameoverScreen::onMenu(CCObject *pSender)
{
    WorldMap* scene = WorldMap::create();
    World* w = SharedConfig::get()->getLastWorld();
    scene->setWorld(w);
    CCDirector::sharedDirector()->replaceScene(CCTransitionFade::create(1.0f, scene));
    if (!GameoverScreen::win) CocosDenshion::SimpleAudioEngine::sharedEngine()->playBackgroundMusic("sound/background.wav", true);
}

void GameoverScreen::onNextLevel(CCObject *pSender)
{
    //Load next level
    GameScreen::nextLevel();

    GameScreen* gamescene = GameScreen::create();
    CCDirector::sharedDirector()->replaceScene(CCTransitionFade::create(1.0f, gamescene));
    if (!GameoverScreen::win) CocosDenshion::SimpleAudioEngine::sharedEngine()->playBackgroundMusic("sound/background.wav", true);
}
