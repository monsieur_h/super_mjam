#include "universescreen.h"
#include "menuscreen.h"
#include "sharedconfig.h"
#include <string>
#include "dirent.h"
#include "world.h"
#include "worldmap.h"
#include "optionmenu.h"

USING_NS_CC;

bool UniverseScreen::init()
{
    CCLayerColor* menuLayer = CCLayerColor::create(BLACK);
    this->addChild(menuLayer);
    _mainMenu = CCMenu::create();
    this->addChild(_mainMenu);

    //Option button
    CCSize visibleSize = CCDirector::sharedDirector()->getVisibleSize();
    const float FONT_SIZE = SharedConfig::get()->menuItemFontSize;
    const float MARGIN = SharedConfig::get()->menuItemMargin;
    CCMenuItemImage* menuButton = CCMenuItemImage::create("option.png",
                                                          "option.png",
                                                          this,
                                                          menu_selector(UniverseScreen::onOptionClicked));
    menuButton->setPositionX(visibleSize.width/2  - menuButton->getContentSize().width + SharedConfig::get()->menuItemMargin/2);
    menuButton->setPositionY(visibleSize.height/2 - menuButton->getContentSize().height - SharedConfig::get()->menuItemMargin/4);
    _mainMenu->addChild(menuButton);

    // Worlds
    std::vector<string> worlds = World::getAvailableWorldNames();
    selectedWorld = 0; //TODO: Charger la derniere position connue
    int lastUnlockedWorldIndex = SharedConfig::get()->getLastWorldIndex();

	for(unsigned int i=0;i<worlds.size();i++)
    {
        string imageName;
        string worldName;
        World* w = new World(worlds[i]);
        if(i <= lastUnlockedWorldIndex)//Si le monde est débloqué
        {
            imageName = w->getPath();
            imageName.append("/world.png");
            worldName = worlds[i];
        }
        else
        {
            imageName = w->getPath();
            imageName.append("/world_locked.png");
            worldName = "locked";
        }

        //Texte
		fonts.push_back(CCMenuItemFont::create(
            worldName.c_str(),
			this,
			menu_selector(UniverseScreen::onWorldClick)));

        //Image
		images.push_back(CCMenuItemImage::create(
            imageName.c_str(),imageName.c_str(),
			this,
			menu_selector(UniverseScreen::onWorldClick)));
    }

	imgSize = (visibleSize.width < visibleSize.height) 
		? visibleSize.width*0.7
		: (visibleSize.height - SharedConfig::get()->menuItemFontSize - SharedConfig::get()->menuItemMargin) *0.7;
	float x = - selectedWorld * (imgSize + SharedConfig::get()->menuItemMargin);
	float y = (SharedConfig::get()->menuItemFontSize + SharedConfig::get()->menuItemMargin + imgSize) / 2 - SharedConfig::get()->menuItemFontSize/2;

    for(unsigned int i=0; i<fonts.size() && i<images.size(); i++)
    {
		fonts[i]->setFontSizeObj(SharedConfig::get()->menuItemFontSize);
		fonts[i]->setFontNameObj(FONT_NAME);
		fonts[i]->setColor(WHITE_3);
        fonts[i]->setPositionX(x);
        fonts[i]->setPositionY(y);
		fonts[i]->setTag(i);
        _mainMenu->addChild(fonts[i]);
		float scale = imgSize / images[i]->getContentSize().width;
		images[i]->setScale(scale);
		images[i]->setScale(scale);
		images[i]->setPositionX(x);
		images[i]->setPositionY(y - SharedConfig::get()->menuItemFontSize - SharedConfig::get()->menuItemMargin - imgSize/2);
		images[i]->setTag(i);
        _mainMenu->addChild(images[i]);
		x += imgSize + SharedConfig::get()->menuItemMargin;
    }
    return true;
}

void UniverseScreen::onWorldClick(CCObject *pSender)
{
    CCMenuItem* pMenuItem = (CCMenuItem *)(pSender);
    int tag = (int)pMenuItem->getTag();
	if(tag == selectedWorld && tag <= SharedConfig::get()->getLastWorldIndex()){
        SharedConfig::get()->onEnterWorld(tag);
        std::vector<string> worlds = World::getAvailableWorldNames();
        string wName = worlds[tag];
        WorldMap* mapScreen = WorldMap::create();
        World* w = new World(wName);
        mapScreen->setWorld(w);
		CCDirector::sharedDirector()->replaceScene(CCTransitionFade::create(1.0f, mapScreen));
	}
	else if (tag != selectedWorld) {
		float move = (tag < selectedWorld)
			? imgSize + SharedConfig::get()->menuItemMargin
			: - (imgSize + SharedConfig::get()->menuItemMargin);
		for(unsigned int i=0; i<fonts.size() && i<images.size(); i++){
			CCMoveBy* slideFont = CCMoveBy::create(0.5, ccp(move, 0));
			fonts[i]->runAction(CCRepeat::create(slideFont, 1));
			CCMoveBy* slideImg = CCMoveBy::create(0.5, ccp(move, 0));
			images[i]->runAction(CCRepeat::create(slideImg, 1));
		}
		selectedWorld = tag;
	}
}

void UniverseScreen::onOptionClicked(CCObject *pSender)
{
    OptionMenu* optionscene = OptionMenu::create();
    CCDirector::sharedDirector()->replaceScene(CCTransitionFade::create(1.0f, optionscene));
}
