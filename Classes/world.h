#ifndef WORLD_H
#define WORLD_H
#include <string>
#include "cocos2d.h"

using namespace std;

/**
 * @brief The World class holds a list of levels and their status (completed, current, score, whatever...)
 */
class World
{
public:
    World(string pName);
    vector<string> getAvailableLevelNames();
    string getPath();
    static vector<string> getAvailableWorldNames();
    string getName();

private:
    string m_name;
    string m_fullPath;
    vector<string> m_levelNames;
    void setAvailableLevelNames();
};

#endif // WORLD_H
