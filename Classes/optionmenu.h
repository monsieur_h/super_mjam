#ifndef OPTIONMENU_H
#define OPTIONMENU_H

#include "cocos2d.h"


class OptionMenu : public cocos2d::CCScene
{
public:
    virtual bool init();
    CREATE_FUNC(OptionMenu);

private:
    void deleteSave();
    void onResumeClicked(CCObject *pSender);
    void onExitClicked(CCObject *pSender);
    void onEraseClicked(CCObject *pSender);
    cocos2d::CCLayerColor*  m_layer;
    cocos2d::CCMenu* m_menu;
};

#endif // OPTIONMENU_H
