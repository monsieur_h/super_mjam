#include "draftline.h"

USING_NS_CC;

DraftLine::DraftLine(CCPoint pP1, CCPoint pP2)
{
    _start = pP1;
    _end = pP2;
    _dashLen = 10;
    _dashSpace = 10;
}

void DraftLine::setLineType(int pType)
{

}

void DraftLine::draw()
{
    ccDrawColor4F(1.0, 1.0, 1.0, 1.0);
    ccDrawLine(
                CCDirector::sharedDirector()->convertToGL(_start),
                CCDirector::sharedDirector()->convertToGL(_end));
}

//CCPoint DraftLine::getWorldStart()
//{
//    return convertToWorldSpace(_start);
//}
