#ifndef DRAFTLINE_H
#define DRAFTLINE_H

#include "cocos2d.h"

class DraftLine : public cocos2d::CCNode
{
public:
    DraftLine(cocos2d::CCPoint p1, cocos2d::CCPoint p2);
    void setLineType(int pType);
    virtual void draw(void);
    cocos2d::CCPoint _start;
    cocos2d::CCPoint _end;

private:

    int _lineType;
    int _dashLen;
    int _dashSpace;
};

#endif // DRAFTLINE_H
