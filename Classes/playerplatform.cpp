#include "playerplatform.h"

USING_NS_CC;

PlayerPlatform::PlayerPlatform(b2World *pWorld, b2Vec2 pStart, b2Vec2 pEnd, float impulseStrength)
{
    _world = pWorld;
    _type  = PLATFORM;
    _start = pStart;
    _end   = pEnd;
    _impulseStrength = impulseStrength;

    b2BodyDef bd;
    bd.type = b2_staticBody;

    bd.position.Set(0, 0);
    b2ChainShape chain;
    b2Vec2 vertices[2];
    vertices[0] = pStart;
    vertices[1] = pEnd;
    chain.CreateChain(vertices, 2);

    b2Body* _body = pWorld->CreateBody(&bd);

    b2FixtureDef fd;
    fd.shape = &chain;
    fd.friction = 1.0f;
    fd.restitution = 1.0f;
    fd.density = 1.0f;	
	fd.filter.categoryBits = PLATFORM;
	fd.filter.maskBits = PLAYER;
    b2Fixture* fix = _body->CreateFixture(&fd);

    fix->SetUserData(this);
    _body->SetUserData(this);
}

PlayerPlatform::~PlayerPlatform()
{
}

std::string PlayerPlatform::getDescription()
{
    return std::string("Type : PLATFORM");
}

b2Body* PlayerPlatform::getBody()
{
    return this->_body;
}
