#ifndef MENUSCREEN_H
#define MENUSCREEN_H

#include "cocos2d.h"
#include "gamescreen.h"

class MenuScreen : public cocos2d::CCScene
{
    public:
        virtual bool init();
        CREATE_FUNC(MenuScreen);

    private :
        cocos2d::CCMenu* _mainMenu;
        void onNewGame(cocos2d::CCObject* pSender);
        void onLoadLevel(cocos2d::CCObject* pSender);
        void onOptions(CCObject* pSender);
        void onExit(CCObject* pSender);
};

#endif // MENUSCREEN_H
