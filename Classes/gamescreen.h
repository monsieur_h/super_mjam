#ifndef GAMESCREEN_H
#define GAMESCREEN_H

#include "cocos2d.h"
#include "player.h"
#include "level.h"
#include "hudlayer.h"
#include <string.h>
#include "animationlayer.h"

class Player;
class AnimationLayer;
class Level;

class GameScreen : public cocos2d::CCScene
{
    public:
        virtual bool init();
        ~GameScreen();
        CREATE_FUNC(GameScreen);
        bool loadLevel(string pLevelName);
        void update(float dt);
        void draw();
        void onTouchBegan(cocos2d::CCPoint pLoc);
        void onTouchEnded(cocos2d::CCPoint pLoc);
        void onTouchMoved(cocos2d::CCPoint pLoc);
        void setPause(bool pPause);
        void addEffect(cocos2d::CCActionInterval* node);

        static void setLevelFile(string levelName);
        static bool isLastLevel();
        static void nextLevel();
    private:
        cocos2d::CCLayerColor* _backgroundLayer;
        AnimationLayer* _animationLayer;
        HudLayer* _hudLayer;
        Player* _player;
        Level* _currentLevel;

        double _elapsedTime;

        void moveCamera(float dt);
        float _screenBorderRatio;//Used to determine wether to move the screen or the player alone...
        float _cameraTrackingSpeed;
};

#endif // GAMESCREEN_H
