#ifndef WORLDMAP_H
#define WORLDMAP_H

#include "cocos2d.h"
#include "world.h"
#include <algorithm>
#include "universescreen.h"

/**
 * @brief The WorldMap class is a screen that presents the levels in a world and the progression of the player through them
 */
class WorldMap : public cocos2d::CCScene
{
public:
    virtual bool init();
    CREATE_FUNC(WorldMap);
    void setWorld(World* pWorld);

private:
    World* m_world;
    void initMenuImages();
    void onOptionClicked(CCObject *pSender);
    void onLevelClicked(cocos2d::CCMenuItemImage* pSender);
	void onBack(CCObject* pSender);
    cocos2d::CCLayerColor*  m_layer;
    cocos2d::CCMenu* m_menu;
};

#endif // WORLDMAP_H
