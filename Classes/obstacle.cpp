#include "obstacle.h"

USING_NS_CC;

Obstacle::Obstacle(b2Body *pBody, b2FixtureDef pFd, unsigned int obstacleType, std::string pName)
{
    _name = pName;
    _type = obstacleType;
    if(_type <= 0)
        _type = OBSTACLE;//DEFAULT
    _body = pBody;
    b2Fixture* fix = _body->CreateFixture(&pFd);
    fix->SetUserData(this);
    _body->SetUserData(this);
    this->_parentLayer = NULL;//Defini dans setImage()


    _startPosition = pBody->GetPosition();
    _endPosition = _startPosition;
    _move.SetZero();
    _moveSpeed = 0;
    _sprite = NULL;
    CCLog("Obstacle created : %s WITH TAG %u RETURNING %u", pName.c_str(),this->_type, this->getBodyType());
}

Obstacle::~Obstacle()
{
    CCLog("Destroying obstacle %s", _name.c_str());
}

std::string Obstacle::getDescription()
{
    return std::string("Type : OBSTACLE\tName : "+_name);
}

void Obstacle::setImage(string pName, CCLayer* pLayer)
{
    this->_parentLayer = pLayer;
    pName.insert(0, "img/");
    CCLog("Setting image %s", pName.c_str());
    _sprite = CCSprite::create(pName.c_str());
    pLayer->addChild(_sprite);
    CCDirector::sharedDirector()->getScheduler()->scheduleUpdateForTarget(this, 0, false);
    resizeImage();
}

void Obstacle::resizeImage()
{
    if(this->_sprite != NULL && this->_body != NULL)
    {
        for( b2Fixture *fixture = this->_body->GetFixtureList(); fixture != NULL; fixture = fixture->GetNext() )
        {
            b2Shape* shape = fixture->GetShape();
            if(shape->GetType() == b2Shape::e_polygon)
            {
                b2PolygonShape* pShape = (b2PolygonShape*) shape;
                int vertexCount = pShape->GetVertexCount();
                b2Vec2* verts = new b2Vec2[vertexCount];
                unsigned int i;
                for(i = 0 ; i < vertexCount ; i++ )
                {
                    verts[i] = pShape->m_vertices[i];
                    verts[i] = this->_body->GetWorldPoint( verts[i] );
                    verts[i] *= PTM_RATIO;
                }

                int minX, maxX, minY, maxY;
                CCSize spriteSize = this->_sprite->getContentSize();
                minX = verts[0].x;
                maxX = verts[0].x;
                minY = verts[0].y;
                maxY = verts[0].y;
                for(i = 0 ; i < vertexCount ; i++)
                {
                    minX = (verts[i].x < minX) ? verts[i].x : minX;
                    minY = (verts[i].y < minY) ? verts[i].y : minY;
                    maxX = (verts[i].x > maxX) ? verts[i].x : maxX;
                    maxY = (verts[i].y > maxY) ? verts[i].y : maxY;
                }

                int distanceX = maxX - minX;
                int distanceY = maxY - minY;
                distanceX = abs(distanceX);
                distanceY = abs(distanceY);
                if((distanceX < spriteSize.width)
                        ||(distanceY < spriteSize.height))
                {
                    int debordementX = distanceX - spriteSize.width;
                    int debordementY = distanceY - spriteSize.height;
                    float ratio = (debordementX < debordementY) ? distanceX / spriteSize.width : distanceY / spriteSize.height;
                    this->_sprite->setScale(ratio);
                }
            }
        }
    }
}

void Obstacle::activate(){
    if(this->_type & CONTACT_DYNAMIC)
    {
        _body->SetGravityScale(1);
    }
}

void Obstacle::activateReverse(){
    if(this->_type & CONTACT_REVERSE)
    {
        _body->SetGravityScale(-1);
    }
}

void Obstacle::update(float dt)
{
    // sprite
    if(_sprite != NULL)
    {
        _sprite->setPosition(ccp(_body->GetPosition().x * PTM_RATIO,
                    _body->GetPosition().y * PTM_RATIO));
        _sprite->setRotation(-1 * CC_RADIANS_TO_DEGREES(_body->GetAngle()));
    }
    // obstacle movant
    if((_startPosition.x != _endPosition.x || _startPosition.y != _endPosition.y) && !this->_dead)
    {
        bool xLimit = false;
        float newX = _body->GetPosition().x;
        if(_endPosition.x < _startPosition.x)
        {
            xLimit = (_body->GetPosition().x <= _endPosition.x 
                || _body->GetPosition().x >= _startPosition.x);
            if(_body->GetPosition().x <= _endPosition.x) newX = _endPosition.x;
            if(_body->GetPosition().x >= _startPosition.x) newX = _startPosition.x;
        }
        else if (_endPosition.x > _startPosition.x)
        {
            xLimit = (_body->GetPosition().x >= _endPosition.x
                || _body->GetPosition().x <= _startPosition.x);
            if(_body->GetPosition().x >= _endPosition.x) newX = _endPosition.x;
            if(_body->GetPosition().x <= _startPosition.x) newX = _startPosition.x;
        }
        bool yLimit = false;
        float newY = _body->GetPosition().y;
        if(_endPosition.y < _startPosition.y)
        {
            yLimit = (_body->GetPosition().y <= _endPosition.y
                || _body->GetPosition().y >= _startPosition.y);
            if(_body->GetPosition().y <= _endPosition.y) newY = _endPosition.y;
            if(_body->GetPosition().y >= _startPosition.y) newY = _startPosition.y;
        }
        else if (_endPosition.y > _startPosition.y)
        {
            yLimit = (_body->GetPosition().y >= _endPosition.y
                || _body->GetPosition().y <= _startPosition.y);
            if(_body->GetPosition().y >= _endPosition.y) newY = _endPosition.y;
            if(_body->GetPosition().y <= _startPosition.y) newY = _startPosition.y;
        }
        if(xLimit) _move.x *= (-1);
        if(yLimit) _move.y *= (-1);

        _body->SetTransform(b2Vec2(newX+_move.x*_moveSpeed*dt,newY+_move.y*_moveSpeed*dt),_body->GetAngle());
    }
}

void Obstacle::setMove(b2Vec2 endPosition, float moveSpeed)
{
    _endPosition = endPosition;
    float32 horSide = _endPosition.x - _startPosition.x;
    float32 vertSide = _endPosition.y - _startPosition.y;
    float32 len = std::sqrt(std::pow(horSide,2) + std::pow(vertSide,2));
    _move.Set(
        (len != 0) ? horSide/len : 0,
        (len != 0) ? vertSide/len : 0);
    _moveSpeed = moveSpeed;
}

void Obstacle::deleteImage()
{
    if(this->_parentLayer != NULL)
        this->_parentLayer->removeChild(_sprite);

    _dead = true;
    _sprite = NULL;
}

void Obstacle::die()
{
    this->deleteImage();
}
