#ifndef OBSTACLE_H
#define OBSTACLE_H

#include "cocos2d.h"
#include "enums.h"
#include "physicalgameobject.h"
#include "Box2D/Box2D.h"

#define PTM_RATIO 32

class Obstacle : public PhysicalGameObject
{
public:
    Obstacle(b2Body* pBody, b2FixtureDef pFd, unsigned int obstacleType, std::string pName="unnamed");
    ~Obstacle();
    std::string getDescription();
    void setImage(string pName, cocos2d::CCLayer* pLayer);
    void deleteImage();
	void activate();
	void activateReverse();
    void update(float dt);
    void setMove(b2Vec2 endPosition, float moveSpeed);
    void die();

private:
    void resizeImage();
    std::string _name;
    cocos2d::CCSprite* _sprite;
    cocos2d::CCNode* _parentLayer;
    bool withSprite;
	int impulseStrenght;
    b2Vec2 _startPosition;
    b2Vec2 _endPosition;
    b2Vec2 _move;
    float _moveSpeed;
};

#endif // OBSTACLE_H
