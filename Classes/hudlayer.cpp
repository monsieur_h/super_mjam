#include "hudlayer.h"
#include "menuscreen.h"
#include "sharedconfig.h"
#include "worldmap.h"
#include "world.h"

USING_NS_CC;

HudLayer::HudLayer() : RELOAD_TIME(2.0f){}

bool HudLayer::init()
{
    //Getting coordinates
    const float FONT_SIZE = SharedConfig::get()->menuItemFontSize;
    const float MARGIN = SharedConfig::get()->menuItemMargin;
    const float HUD_MARGIN = SharedConfig::get()->hudElementMargin;
    CCSize visibleSize = CCDirector::sharedDirector()->getVisibleSize();
    CCPoint origin = CCDirector::sharedDirector()->getVisibleOrigin();

    _menu = CCMenu::create();
    this->addChild(_menu, 1);

    _openMenu = CCMenuItemImage::create(
                            "menu/pause.png",
                            "menu/play.png",
                            this,
                            menu_selector(HudLayer::onShowMenu) );

    _openMenu->setAnchorPoint(ccp(0, 1));
    _openMenu->setScale(0.35f);
    _openMenu->setPosition(ccp(
        - visibleSize.width/2 + MARGIN,
          visibleSize.height/2 - MARGIN));
    _menu->addChild(_openMenu);


    //Pause menu
	float yPos = (FONT_SIZE + MARGIN) / 2;

    this->_pauseMenu = CCMenu::create();
    this->_pauseMenu->setAnchorPoint(ccp(0, 0));
    this->addChild(_pauseMenu);

    CCMenuItemFont* restartButton = CCMenuItemFont::create(
        "Restart",
        this,
        menu_selector(HudLayer::onRestartButtonPressed));
	restartButton->setColor(WHITE_3);
	restartButton->setFontNameObj(FONT_NAME);
    restartButton->setFontSizeObj(FONT_SIZE);
    restartButton->setPositionY(yPos);
	yPos -= FONT_SIZE + MARGIN;

    CCMenuItemFont* returnButton = CCMenuItemFont::create(
        "World map",
        this,
        menu_selector(HudLayer::onReturnButtonPressed));
	returnButton->setColor(WHITE_3);
	returnButton->setFontNameObj(FONT_NAME);
    returnButton->setFontSizeObj(FONT_SIZE);
    returnButton->setPositionY(yPos);

    _pauseMenu->addChild(restartButton);
    _pauseMenu->addChild(returnButton);
    _pauseMenu->setVisible(false);
    this->setPause(false);


    const int labelFontSize = FONT_SIZE / 2;
    _scoreLabel = CCLabelTTF::create("Get Ready !", FONT_NAME, labelFontSize);



    _scoreLabel->setPositionY(visibleSize.height - labelFontSize / 2 - MARGIN);
    _scoreLabel->setPositionX(visibleSize.width / 2);
    this->addChild(_scoreLabel, 2);
    return true;
}

void HudLayer::onShowMenu(CCObject *pSender)
{
    this->setPause(!this->_isPaused);
}

void HudLayer::onReturnButtonPressed(CCObject* pSender)
{
    //unpause the game
    WorldMap* scene = WorldMap::create();
    World* w = SharedConfig::get()->getLastWorld();
    scene->setWorld(w);
    CCDirector::sharedDirector()->replaceScene(CCTransitionFade::create(1.0f, scene));
}

void HudLayer::onRestartButtonPressed(CCObject* pSender)
{
    GameScreen* gamescene = GameScreen::create();
    CCDirector::sharedDirector()->replaceScene(CCTransitionFade::create(1.0f, gamescene));
}

void HudLayer::unpause()
{
    this->setPause(false);
}

void HudLayer::setPause(bool pPause)
{
    this->_isPaused = pPause;
    if(pPause)
        this->_openMenu->selected();
    else
        this->_openMenu->unselected();

    CCLog("Setting pause to %d", pPause);
    const float duration = 0.3f; //Duration of the animation
    const float maxScale = 2.0f;

    CCActionInterval* fading;
    CCActionInterval* scaling;
    if(pPause)
    {
        this->_pauseMenu->setVisible(true);
        fading = CCFadeIn::create(duration * 0.9); //Fading 10% shorter than scaling
        this->_pauseMenu->setScale(maxScale);
        CCActionInterval* scalingAction= CCScaleTo::create(duration, 1.0f);
        scaling = CCEaseBackOut::create(scalingAction);
    }
    else
    {
        fading = CCFadeOut::create(duration * 0.9);
        CCActionInterval* scalingAction= CCScaleTo::create(duration, maxScale);
        scaling = CCEaseBackIn::create((CCActionInterval*)(scalingAction->copy()->autorelease()));
    }
    _pauseMenu->runAction(fading);
    _pauseMenu->runAction(scaling);
    _pauseMenu->setEnabled(pPause);
}

bool HudLayer::isPaused()
{
    return this->_isPaused;
}

void HudLayer::setScoreLabel(std::string pStr)
{
    if(!pStr.empty())
        this->_scoreLabel->setString(pStr.c_str());
}
