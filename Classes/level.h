#ifndef LEVEL_H
#define LEVEL_H

#include "cocos2d.h"
#include <string>
#include <fstream>
#include "Box2D/Box2D.h"
#include "ext/pugixml/pugixml.hpp"
#include "ext/pugixml/pugiconfig.hpp"
#include "collisionmanager.h"
#include "obstacle.h"
#include <cmath>
#include <vector>

#define PTM_RATIO 32

class CollisionManager;

struct bodyContainer;
class Level
{
    //Used to store indexed bodies (to make Joints)


    public:
        Level(string pFileName, cocos2d::CCLayer* pLayer);
        void update(float dt);
        static bool exists(const char *pFileName);
        static string findFullPath(const char* pLevelFileName);
        static std::vector<std::string> getAvailableLevelNames();
        b2World* getWorld();
        ~Level();
        int getMaxPlatforms();
		float linearDamping;
		float angularDamping;
		float impulseStrength;
		float restitution;

    private:
        b2World* _world;
        CollisionManager* _collisionManager;
        string _filename;
        string _levelName;
        int32 _velocityIterations;
        int32 _positionIterations;
        std::vector <Obstacle*> _obstacleList;
        int _maxPlatforms;
        void loadWorldFromNode(pugi::xml_node pWorldNode);
        void loadWorldObjectFromNode(pugi::xml_node pObjectNode);
        void loadJointFromNode(pugi::xml_node pJointNode);
        static b2PolygonShape createBoxShapeFromNode(pugi::xml_node pShapeNode);
        static b2CircleShape createCircleShapeFromNode(pugi::xml_node pShapeNode);
        static void createChainShapeFromNode(pugi::xml_node pShapeNode, b2ChainShape &shape);
        static void createPolygonShapeFromNode(pugi::xml_node pShapeNode, b2PolygonShape &shape);
        static double toRadians(double pAngle);
        static double toDegrees(double pAngle);
        cocos2d::CCLayer* _mainLayer;
        Obstacle* _goal;
        bool _finished;

        std::vector<bodyContainer> indexedBodyList;
        bodyContainer* findBodyByIndex(int pIndex);
};

#endif // LEVEL_H
