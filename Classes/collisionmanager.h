#ifndef COLLISIONMANAGER_H
#define COLLISIONMANAGER_H

#include "cocos2d.h"
#include "Box2D/Box2D.h"
#include "enums.h"
#include "physicalgameobject.h"
#include "playerplatform.h"
#include "player.h"
#include "obstacle.h"

#define PTM_RATIO 32

class CollisionManager : public b2ContactListener
{
public:
    CollisionManager(b2World* pWorld);
    void BeginContact(b2Contact* pContact);
    void cleanupDeadBodies();
    void EndContact(b2Contact *pContact);
//    void PreSolve(b2Contact *contact, const b2Manifold *oldManifold);


private:
    b2World* _world;
    std::vector <b2Body*> _toBeDestroyed;
    void safelyKill(PhysicalGameObject* pObject);
    void applyImpulse(b2Body* pBody, b2Vec2 pStart, b2Vec2 pEnd, float pStren);
	void applyImpulse(b2Body* pBody, b2Body* circle);
};

#endif // COLLISIONMANAGER_H
