#include "sharedconfig.h"
USING_NS_CC;

SharedConfig::SharedConfig()
{
    menuItemFontSize = 45;
    menuItemMargin = 10;
    menuTitleFontSize = 75;
    hudElementMargin = 20;

    _currentLevel = 0;
    _currentWorld = 0;
    _lastWorld = 0;
    _lastLevel = 0;
    readFromFile();
}

SharedConfig* SharedConfig::get()
{
    static SharedConfig _instance;
    return &_instance;
}

SharedConfig* SharedConfig::getInstance()
{
    return SharedConfig::get();
}

SharedConfig::~SharedConfig()
{
//    delete _instance;
}

void SharedConfig::onEnterWorld(int index)
{
    cocos2d::CCLog("Entering world %d", index);
    this->_currentWorld = index;
}

void SharedConfig::onEnterLevel(int index)
{
    cocos2d::CCLog("Entering level %d", index);
    this->_currentLevel = index;
}

World* SharedConfig::getLastWorld()
{
    vector<string> list = World::getAvailableWorldNames();

    World* w = new World(list[this->_lastWorld].c_str());
    return w;
}

World* SharedConfig::getCurrentWorld()
{
    vector<string> list = World::getAvailableWorldNames();

    World* w = new World(list[this->_currentWorld].c_str());
    return w;
}

int SharedConfig::getCurrentWorldIndex()
{
    return this->_currentWorld;
}

World* SharedConfig::getNextWorld()
{
    vector<string> list = World::getAvailableWorldNames();

    int index;
    index = ((this->_currentWorld +1) > (list.size() -1)) ? (list.size() -1) : (this->_currentWorld + 1);
    World* w = new World(list[index].c_str());
    return w;
}

int SharedConfig::getLastWorldIndex()
{
    return this->_lastWorld;
}

int SharedConfig::getLastLevelIndex(int worldIndex)
{
    if(worldIndex > this->getLastWorldIndex())
    {
        return 0;
    }
    else if(worldIndex == this->getLastWorldIndex())
    {
        return this->_lastLevel;
    }
    else
    {
        return this->getLastWorld()->getAvailableLevelNames().size();
    }
}

bool SharedConfig::isLastLevel()
{
    World* w = SharedConfig::get()->getCurrentWorld();
    vector<string> levelList = w->getAvailableLevelNames();
    string lastLevel(levelList[levelList.size()-1]);
    if(lastLevel == levelList[this->_currentLevel])
    {
        return true;
    }
    else
    {
        return false;
    }
}

int SharedConfig::getCurrentLevel()
{
    return this->_currentLevel;
}

void SharedConfig::onLevelOver(bool isWon)
{
    cocos2d::CCLog("Current level (%d:%d) is over", _currentWorld, _currentLevel);
    if(isWon)
    {
        if(this->isLastLevel())
        {
            this->_lastWorld++;
            this->_lastLevel = 0;
        }
        else
        {
            this->_lastLevel++;
        }
    }
    cocos2d::CCLog("Last level : (%d:%d)", _lastWorld, _lastLevel);
    saveToFile();
    //TODO:else -> compter le nombre d'échecs?
}

void SharedConfig::saveToFile()
{
    CCUserDefault* ud = CCUserDefault::sharedUserDefault();
    ud->setIntegerForKey("lastLevel", this->_lastLevel);
    ud->setIntegerForKey("lastWorld", this->_lastWorld);
    ud->flush();
}

void SharedConfig::readFromFile()
{
    CCUserDefault* ud = CCUserDefault::sharedUserDefault();
    this->_lastLevel = ud->getIntegerForKey("lastLevel");
    this->_lastWorld = ud->getIntegerForKey("lastWorld");
}

void SharedConfig::resetSave()
{
    this->_lastLevel = 0;
    this->_lastWorld = 0;
    this->saveToFile();
}
