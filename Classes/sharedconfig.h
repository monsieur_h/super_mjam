#ifndef SHAREDCONFIG_H
#define SHAREDCONFIG_H
#include "world.h"
#include "cocos2d.h"
/**
 * @brief The SharedConfig class shares data and config accross the whole project
 */

#define BLACK ccc4(20,20,20,255)
#define WHITE ccc4(235,235,235,255)
#define RED ccc4(221,48,48,255)
#define GREEN ccc4(26,140,38,255)
#define ORANGE ccc4(237,145,16,255)
#define BLACK_3 ccc3(20,20,20)
#define WHITE_3 ccc3(235,235,235)
#define RED_3 ccc3(221,48,48)
#define GREEN_3 ccc3(26,140,38)
#define ORANGE_3 ccc3(237,145,16)
#define BLACK_F b2Color((float)20/255,(float)20/255,(float)20/255)
#define WHITE_F b2Color((float)235/255,(float)235/255,(float)235/255)
#define RED_F b2Color((float)221/255,(float)48/255,(float)48/255)
#define GREEN_F b2Color((float)26/255,(float)140/255,(float)38/255)
#define ORANGE_F b2Color((float)237/255,(float)145/255,(float)16/255)
#define FONT_NAME "JD Equinox"

class SharedConfig
{

public:
    static SharedConfig* get();
    static SharedConfig* getInstance();//Alias

    void onEnterWorld(int index);
    void onEnterLevel(int index);
    World* getLastWorld();
    World* getCurrentWorld();
    World* getNextWorld();
    int getLastWorldIndex();
    int getLastLevelIndex(int worldIndex);
    int getCurrentWorldIndex();
    float menuItemFontSize;
    float menuItemMargin;
    float menuTitleFontSize;
    float hudElementMargin;
    bool isLastLevel();
    int getCurrentLevel();
    void onLevelOver(bool isWon);
    void readFromFile();
    void saveToFile();
    void resetSave();


private:
    SharedConfig();
    ~SharedConfig();
    SharedConfig(SharedConfig const &);
    void operator=(SharedConfig const &);
    static SharedConfig _instance;
    int _currentWorld, _currentLevel;
    int _lastWorld, _lastLevel;

};

#endif // SHAREDCONFIG_H
