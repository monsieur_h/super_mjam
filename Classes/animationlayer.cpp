#include "animationlayer.h"
#include "sharedconfig.h"

USING_NS_CC;

bool AnimationLayer::init()
{
    setTouchEnabled(true);
    this->_firstPlatformPlaced = false;
    this->initShader();
    this->_platformCount = 0;
    return true;
}

void AnimationLayer::initShader()
{
    glEnable(GL_LINE_SMOOTH);
    mShaderProgram = CCShaderCache::sharedShaderCache()->programForKey(kCCShader_Position_uColor);

    mColorLocation = glGetUniformLocation( mShaderProgram->getProgram(), "u_color");
}

AnimationLayer::AnimationLayer()
{
    _currentLine = new DraftLine(ccp(0,0), ccp(0,0));
    setTouchEnabled(true);
    _platform = NULL;
}

void AnimationLayer::ccTouchesBegan(CCSet *pTouches, CCEvent *pEvent)
{
    CCSetIterator it = pTouches->begin();
    CCTouch* touch = (CCTouch*) (*it);
    _currentLine->_start = touch->getLocationInView();
    _currentLine->_end   = touch->getLocationInView();

}

void AnimationLayer::ccTouchesMoved(CCSet *pTouches, CCEvent *pEvent)
{
    CCSetIterator it = pTouches->begin();
    CCTouch* touch = (CCTouch*) (*it);
    _currentLine->_end = touch->getLocationInView();
}

void AnimationLayer::ccTouchesEnded(CCSet *pTouches, CCEvent *pEvent)
{
    CCSetIterator it = pTouches->begin();
    CCTouch* touch   = (CCTouch*) (*it);
    CCPoint loc = touch->getLocationInView();
    _currentLine->_end = loc;
    if(_platform != NULL)
    {
        _platform->die();
    }

    createPlayerPlatform(_currentLine->_start, _currentLine->_end);
    this->_firstPlatformPlaced = true;

    //Cleaning the line
    _currentLine->_start = ccp(0, 0);
    _currentLine->_end   = ccp(0, 0);
}

void AnimationLayer::createPlayerPlatform(CCPoint pP1, CCPoint pP2)
{
    CCPoint start = CCDirector::sharedDirector()->convertToGL(pP1);
    CCPoint end = CCDirector::sharedDirector()->convertToGL(pP2);
    start = start - this->getPosition();
    end = end - this->getPosition();


    //Conversion to b2Vec2
    b2Vec2 p1(start.x/PTM_RATIO, start.y/PTM_RATIO);
    b2Vec2 p2(end.x/PTM_RATIO, end.y/PTM_RATIO);
	_platform = new PlayerPlatform(_level->getWorld(), p1, p2, _level->impulseStrength);
    this->_platformCount++;
}

AnimationLayer::~AnimationLayer()
{
};

void AnimationLayer::draw()
{
    //Drawing the platform line
    _currentLine->draw();

    this->drawShapes();
}

void AnimationLayer::addLevel(Level *pLevel)
{
    _level = pLevel;
}

bool AnimationLayer::isFistPlatformPlaced()
{
    return this->_firstPlatformPlaced;
}

void AnimationLayer::drawShapes()
{
    if(_level == NULL)
        return;
    //Drawing the shapes for dangerous platforms
    unsigned int allowedTags = 0xFFFFF;
    b2World* world = this->_level->getWorld();

    if(world == NULL)
        return;

    b2Body* node = this->_level->getWorld()->GetBodyList();
    int j;
    while(node != NULL)
    {
        b2Body* body = node;
        node = node->GetNext();
        PhysicalGameObject* obj = (PhysicalGameObject*) body->GetUserData();
        unsigned int tagObj = obj->getBodyType();

        if((obj != NULL))
        {
			for( b2Fixture *fixture = body->GetFixtureList(); fixture != NULL; fixture = fixture->GetNext() )
            {
                b2Shape* shape = fixture->GetShape();

				//Polygones
                if(shape->GetType() == b2Shape::e_polygon)
                {
                    drawPolygoneShape(fixture, body, tagObj);
                }

                //Cercles
                if(shape->GetType() == b2Shape::e_circle)
                {
                    drawCircleShape(fixture, body, tagObj);
                }

                //Chaines
                if(shape->GetType() == b2Shape::e_chain)
                {
                    drawChainShape(fixture, body, tagObj);
                }
			}
		}
    }
}

void AnimationLayer::configureOpenGl(unsigned int type, bool triangleFan = false)
{
    mShaderProgram->use();
    mShaderProgram->setUniformsForBuiltins();

    glLineWidth(2.f);
    cocos2d::ccColor4B color = WHITE;
    if((type & OBSTACLE) != 0)
    {
        color = WHITE;
    }
    else if((type & DANGEROUS) != 0)
    {
        color = RED;
    }
    else if((type & CONTACT_IMPULSE) != 0)
    {
        color = WHITE;
    }
    else if((type & GOAL) != 0)
    {
        if(triangleFan == false)
            color = BLACK;
        else
            color = WHITE;
    }
    else if((type & PLAYER) != 0)
    {
        color = WHITE;
    }

    if((type & CONTACT_DISSAPEAR) != 0)
    {
        glEnable(GL_LINE_STIPPLE);
        glLineStipple(1, 0xFF00);
    }
    else
    {
        glDisable(GL_LINE_STIPPLE);
    }

    //Si c' est pour remplir une forme, baisser l'alpha
    if(triangleFan == true)
    {
        if((type & PLAYER) == 0)
            color.a /= 8;
    }

    glUniform4f(mColorLocation, color.r/255.f, color.g/255.f, color.b/255.f, color.a/255.f);
    mShaderProgram->setUniformLocationWith4f(mColorLocation, color.r/255.f, color.g/255.f, color.b/255.f, color.a/255.f);
}

void AnimationLayer::drawChainShape(b2Fixture *fixture, b2Body *body, unsigned int type)
{
    b2ChainShape* pShape = (b2ChainShape*) fixture->GetShape();
    int vertexCount = pShape->m_count;
    int i;
    b2Vec2* verts = new b2Vec2[vertexCount];

    for(i = 0 ; i < vertexCount ; i++ )
    {
        verts[i] = pShape->m_vertices[i];
        verts[i] = body->GetWorldPoint( verts[i] );
        verts[i] *= PTM_RATIO;
    }

    configureOpenGl(type);

    glVertexAttribPointer(kCCVertexAttrib_Position, 2, GL_FLOAT, GL_FALSE, 0, verts);

    glDrawArrays(GL_LINE_STRIP, 0, vertexCount);

    CC_INCREMENT_GL_DRAWS(2);

    CHECK_GL_ERROR_DEBUG();
    delete[] verts;
}


void AnimationLayer::drawPolygoneShape(b2Fixture* fixture, b2Body* body, unsigned int type)
{
    if((type & DESCRIPTION) != 0 )
    {
        //Ne pas dessiner le trait pour les objets de type description
        return;
    }
    b2PolygonShape* pShape = (b2PolygonShape*) fixture->GetShape();
    int vertexCount = pShape->GetVertexCount();
    int i;
    b2Vec2* verts = new b2Vec2[vertexCount];

    for(i = 0 ; i < vertexCount ; i++ )
    {
        verts[i] = pShape->GetVertex(i);
        verts[i] = body->GetWorldPoint( verts[i] );
        verts[i] *= PTM_RATIO;
    }

    //Draw config
    configureOpenGl(type);

    glVertexAttribPointer(kCCVertexAttrib_Position, 2, GL_FLOAT, GL_FALSE, 0, verts);

    //Dessin du contour
    glDrawArrays(GL_LINE_LOOP, 0, vertexCount);

    //Dessin de la forme pleine
    configureOpenGl(type, true);
    glDrawArrays(GL_TRIANGLE_FAN, 0, vertexCount);


    CC_INCREMENT_GL_DRAWS(2);

    CHECK_GL_ERROR_DEBUG();
    delete[] verts;
}

void AnimationLayer::drawCircleShape(b2Fixture* fixture, b2Body* body, unsigned int type)
{
    b2CircleShape* pShape = (b2CircleShape*) fixture->GetShape();
    b2Vec2 center = body->GetWorldCenter();
    center *= PTM_RATIO;
    const float32 k_segments = 16.0f;
    int vertexCount=16;
    const float32 k_increment = 2.0f * b2_pi / k_segments;
    float32 theta = 0.0f;
    const int radius = pShape->m_radius * PTM_RATIO;

    b2Vec2*    glVertices = new b2Vec2[vertexCount];
    for (int i = 0; i < k_segments; ++i)
    {
        glVertices[i] = center +( radius * b2Vec2(cosf(theta), sinf(theta)));
        theta += k_increment;
    }

    configureOpenGl(type);
    glVertexAttribPointer(kCCVertexAttrib_Position, 2, GL_FLOAT, GL_FALSE, 0, glVertices);

    glDrawArrays(GL_LINE_LOOP, 0, vertexCount);

    configureOpenGl(type, true);
    glDrawArrays(GL_TRIANGLE_FAN, 0, vertexCount);

    CC_INCREMENT_GL_DRAWS(1);
	CHECK_GL_ERROR_DEBUG();

    delete[] glVertices;
}

int AnimationLayer::getPlatformCount()
{
    return this->_platformCount;
}

void AnimationLayer::addEffect(cocos2d::CCNode *node)
{
    this->addChild(node);
}
