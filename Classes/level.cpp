#include "level.h"
#include "gameoverscreen.h"
#include "dirent.h"

USING_NS_CC;
using namespace pugi;

struct bodyContainer{
    b2Body* body;
    int id;
};

Level::Level(string pFileName, CCLayer* pLayer) : _filename(pFileName),
    _velocityIterations(8),
    _positionIterations(1),
    _maxPlatforms(1),
    _mainLayer(pLayer),
    _finished(false)
{
    unsigned long bufferSize = 0;
    unsigned char* pBuffer;
    pBuffer = CCFileUtils::sharedFileUtils()->getFileData(_filename.c_str(), "r", &bufferSize);

    indexedBodyList.clear();

    CCLog("Read file %s for size %luo", pFileName.c_str(), bufferSize);

    xml_document xmlDoc;
//    xml_parse_result xmlContent = xmlDoc.load_file(filenameWithFullPath.c_str());
    xml_parse_result xmlContent = xmlDoc.load_buffer(pBuffer, bufferSize);
    if(!xmlContent)
    {
        CCLog("Error : Can't parse XML file for level");
        return;//TODO: Raise here too
    }
    CCLog("Loading level %s...", xmlDoc.child("level").attribute("name").value());

    this->loadWorldFromNode(xmlDoc.child("level").child("world"));
    CCLog("Loading complete !");
}

void Level::loadWorldFromNode(xml_node pWorldNode)
{
    //First step, create the b2World
    _levelName = pWorldNode.attribute("name").as_string();
    xml_node gravityNode = pWorldNode.child("gravity");
    b2Vec2 gravityVector(gravityNode.attribute("x").as_double(),
                   gravityNode.attribute("y").as_double());

    CCLog("Creating world...");
    _world = NULL;
    CCLog("World @ %p", _world);
    _world = new b2World(gravityVector);
    CCLog("World @ %p", _world);
    _collisionManager = new CollisionManager(_world);
    _world->SetContactListener(_collisionManager);

	//Change the parrameter of the player
	xml_node playerNode = pWorldNode.child("player");	
    linearDamping = playerNode.attribute("linear_damping").as_double();
    angularDamping = playerNode.attribute("angular_damping").as_double();
	impulseStrength = playerNode.attribute("impulse_strength").as_double();
	restitution = playerNode.attribute("restitution").as_double();



    //Now load all the objects in it
    for(xml_node objectNode = pWorldNode.child("worldObjects").child("object");
        objectNode!=NULL;
        objectNode = objectNode.next_sibling("object"))
    {
        loadWorldObjectFromNode(objectNode);
    }

    //Create all the joints
    for(xml_node jointNode = pWorldNode.child("worldObjects").child("joint");
        jointNode != NULL;
        jointNode = jointNode.next_sibling("joint"))
    {
        loadJointFromNode(jointNode);
    }
}

void Level::loadJointFromNode(xml_node pJointNode)
{
    bodyContainer *bodyContA, *bodyContB;
    b2Vec2 anchorA, anchorB;
    xml_node nodeA, nodeB;
    string jointType;
    if(!pJointNode.attribute("type"))
        return;

    jointType = pJointNode.attribute("type").as_string();
    CCLog("Joint type found %s", jointType.c_str());
    if(jointType == "revolute")
    {
        //XML Nodes
        nodeA = pJointNode.child("element1");
        nodeB = pJointNode.child("element2");

        //Body containers
        bodyContA = this->findBodyByIndex(nodeA.attribute("id").as_int());
        bodyContB = this->findBodyByIndex(nodeB.attribute("id").as_int());

        //Anchors
        anchorA.x = nodeA.attribute("local_anchor_x").as_double() / PTM_RATIO;
        anchorA.y = nodeA.attribute("local_anchor_y").as_double() / PTM_RATIO;

        anchorB.x = nodeB.attribute("local_anchor_x").as_double() / PTM_RATIO;
        anchorB.y = nodeB.attribute("local_anchor_y").as_double() / PTM_RATIO;

        CCLog("TEST");

        b2RevoluteJointDef jointDef;

        jointDef.localAnchorA = anchorA;
        jointDef.localAnchorB = anchorB;

        jointDef.bodyA = bodyContA->body;
        jointDef.bodyB = bodyContB->body;

        b2RevoluteJoint* joint = (b2RevoluteJoint*)this->_world->CreateJoint(&jointDef);

        joint = NULL;
    }
}

void Level::loadWorldObjectFromNode(xml_node pObjectNode)
{
    //Body
    xml_node bodyNode = pObjectNode.child("body");
    CCLog("Creating body for object %s", pObjectNode.attribute("name").as_string());
    b2BodyDef bd;
    string sBodyType = bodyNode.attribute("type").as_string();
    if(sBodyType == "dynamic")
    {
        bd.type = b2_dynamicBody;
    }
    else if(sBodyType == "static")
    {
        bd.type = b2_staticBody;
    }
    else if(sBodyType == "kinematic")
    {
        bd.type = b2_kinematicBody;
    }
    else
    {
        bd.type = b2_dynamicBody;
    }

    if(bodyNode.attribute("angular_damping"))
    {
        bd.angularDamping = bodyNode.attribute("angular_damping").as_double();
    }

    if(bodyNode.attribute("linear_damping"))
    {
        bd.linearDamping = bodyNode.attribute("linear_damping").as_double();
    }
	

    bd.position.Set(bodyNode.attribute("x").as_double()/PTM_RATIO,
                    bodyNode.attribute("y").as_double()/PTM_RATIO);
    b2Body* body = _world->CreateBody(&bd);

    if(pObjectNode.attribute("id"))
    {
        bodyContainer b;
        b.body = body;
        b.id = pObjectNode.attribute("id").as_int();
        indexedBodyList.push_back(b);
    }

	//dit si un objet n'est soumis � aucune gravit�
	if (bodyNode.attribute("floating").as_bool())
	{
		body->SetGravityScale(0);
	}
	
    unsigned int obstacleType = 0;

	//d�termine si l'obstacle est particulier
    std::vector<std::string> listTag;
	if (bodyNode.attribute("tag"))
	{
		std::string tags = bodyNode.attribute("tag").as_string();
		std::istringstream iss(tags);

		for (std::string token; std::getline(iss, token, ';'); )
        {
            listTag.push_back(token);
		}
		//d�termine si un obstacle est soumis � la gravit� au contacte
		if(std::find(listTag.begin(), listTag.end(), "contactDynamic") != listTag.end()) {
            obstacleType = obstacleType | CONTACT_DYNAMIC;
			body->SetGravityScale(0);
		}
		//d�termine si un obstacle est soumis � la gravit� invers� au contacte
		if(std::find(listTag.begin(), listTag.end(), "contactReverse") != listTag.end()) {
            obstacleType = obstacleType | CONTACT_REVERSE;
			body->SetGravityScale(0);
		}
		//d�termine si un obstacle fait une impulsion
		if(std::find(listTag.begin(), listTag.end(), "impulse") != listTag.end()) {
			obstacleType = obstacleType | CONTACT_IMPULSE;
		}
		//d�termine si un obstacle disparait � l'impacte
		if(std::find(listTag.begin(), listTag.end(), "disappear") != listTag.end()) {
			obstacleType = obstacleType | CONTACT_DISSAPEAR;
		}
		//d�termine si un obstacle est dangereux
		if(std::find(listTag.begin(), listTag.end(), "dangerous") != listTag.end()) {
            CCLog("Creating a dangerous one %u", obstacleType);
            obstacleType = obstacleType | DANGEROUS;
            CCLog("Is now %u", obstacleType);
		}
	}	

    //Fixture & parameters
    xml_node fixtureNode = pObjectNode.child("fixture");
    b2FixtureDef fd;
	
	fd.filter.categoryBits = OBSTACLE;
	fd.filter.maskBits = PLAYER | OBSTACLE;

    if(fixtureNode.attribute("density"))
    {
        fd.density = fixtureNode.attribute("density").as_double();
    }
    if(fixtureNode.attribute("friction"))
    {
        fd.friction = fixtureNode.attribute("friction").as_double();
    }
    if(fixtureNode.attribute("restitution"))
    {
        fd.restitution = fixtureNode.attribute("restitution").as_double();
    }

    //d�termine si c' est un obstacle fictif
    if(std::find(listTag.begin(), listTag.end(), "description") != listTag.end())
    {
        fd.isSensor = true;
        obstacleType = obstacleType | DESCRIPTION;
    }

    //Shape
    xml_node shapeNode = pObjectNode.child("shape");
    string sShapeType = shapeNode.attribute("type").as_string();
    string nodeName = pObjectNode.attribute("name").as_string();
    if (nodeName == "goal")
    {
        b2PolygonShape sh;
        sh = Level::createBoxShapeFromNode(shapeNode);
        fd.shape = &sh;
        fd.isSensor = true;
        obstacleType = obstacleType | GOAL;
        _goal = new Obstacle(body, fd, obstacleType, pObjectNode.attribute("name").as_string());
		_goal->setImage("goal.png", _mainLayer);
		_obstacleList.push_back(_goal);
    }
    else if(sShapeType == "box")
    {
        b2PolygonShape sh;
        sh = Level::createBoxShapeFromNode(shapeNode);
        fd.shape = &sh;
//        body->CreateFixture(&fd);
        Obstacle* o = new Obstacle(body, fd, obstacleType, pObjectNode.attribute("name").as_string());
        _obstacleList.push_back(o);
        if(pObjectNode.attribute("image"))
        {
            o->setImage(pObjectNode.attribute("image").as_string(), _mainLayer);
        }
        else if((obstacleType & CONTACT_DYNAMIC) != 0)
        {
            o->setImage("arrowdown.png", _mainLayer);
        }
        else if((obstacleType & CONTACT_REVERSE) != 0)
        {
            o->setImage("arrowup.png", _mainLayer);
        }
		else if((obstacleType & CONTACT_IMPULSE) != 0)
        {
            o->setImage("spring.png", _mainLayer);
        }
    }
    else if(sShapeType == "circle")
    {
        b2CircleShape sh;
        sh = Level::createCircleShapeFromNode(shapeNode);
        fd.shape = &sh;
//        body->CreateFixture(&fd);
        Obstacle* o = new Obstacle(body, fd, obstacleType, pObjectNode.attribute("name").as_string());
        _obstacleList.push_back(o);
        if(pObjectNode.attribute("image"))
        {
            o->setImage(pObjectNode.attribute("image").as_string(), _mainLayer);
        }
		else if((obstacleType & CONTACT_DYNAMIC) != 0)
        {
            o->setImage("arrowdown.png", _mainLayer);
        }
        else if((obstacleType & CONTACT_REVERSE) != 0)
        {
            o->setImage("arrowup.png", _mainLayer);
        }
		else if((obstacleType & CONTACT_IMPULSE) != 0)
        {
            o->setImage("spring.png", _mainLayer);
        }
    }
    else if(sShapeType == "chain")
    {
        b2ChainShape sh;
        Level::createChainShapeFromNode(shapeNode, sh);
        fd.shape = &sh;
        Obstacle* o = new Obstacle(body, fd, obstacleType, pObjectNode.attribute("name").as_string());
        _obstacleList.push_back(o);
        if(pObjectNode.attribute("image"))
        {
            o->setImage(pObjectNode.attribute("image").as_string(), _mainLayer);
        }
    }//TODO: MOAR SHAPES !
    // moving obstacle
    if(bodyNode.attribute("endx") && bodyNode.attribute("endy") && bodyNode.attribute("movespeed")){
        _obstacleList[_obstacleList.size()-1]->setMove(
            b2Vec2(bodyNode.attribute("endx").as_float()/PTM_RATIO,
                bodyNode.attribute("endy").as_float()/PTM_RATIO),
            bodyNode.attribute("movespeed").as_float());
		_obstacleList[_obstacleList.size()-1]->setImage("arrowleftright.png", _mainLayer);
    }
    else if(sShapeType == "polygon")
    {
        b2PolygonShape sh;
        Level::createPolygonShapeFromNode(shapeNode, sh);
        fd.shape = &sh;
        Obstacle* o = new Obstacle(body, fd, obstacleType, pObjectNode.attribute("name").as_string());
        _obstacleList.push_back(o);
        if(pObjectNode.attribute("image"))
        {
            o->setImage(pObjectNode.attribute("image").as_string(), _mainLayer);
        }
		else if((obstacleType & CONTACT_DYNAMIC) != 0)
        {
            o->setImage("arrowdown.png", _mainLayer);
        }
        else if((obstacleType & CONTACT_REVERSE) != 0)
        {
            o->setImage("arrowup.png", _mainLayer);
        }
		else if((obstacleType & CONTACT_IMPULSE) != 0)
        {
            o->setImage("spring.png", _mainLayer);
        }
    }
}

void Level::createPolygonShapeFromNode(xml_node pShapeNode, b2PolygonShape &shape)
{
    int vertCount = std::distance(pShapeNode.children().begin(), pShapeNode.children().end());

    CCLog("Creating a polygonShape for %d vertices", vertCount);
    b2Vec2* vertices = new b2Vec2[vertCount];

    int i = 0;
    for(xml_node_iterator it = pShapeNode.children().begin();
        it != pShapeNode.children().end();
        ++it)
    {
        vertices[i] = b2Vec2 (it->attribute("x").as_double() / PTM_RATIO,
                              it->attribute("y").as_double() / PTM_RATIO);
        i++;
    }
    shape.Set(vertices, vertCount);
}

void Level::createChainShapeFromNode(xml_node pShapeNode, b2ChainShape &shape)
{
    int vertCount = std::distance(pShapeNode.children().begin(), pShapeNode.children().end());

    CCLog("Creating a chainShape for %d vertices", vertCount);
    b2Vec2* vertices = new b2Vec2[vertCount];

    int i = 0;
    for(xml_node_iterator it = pShapeNode.children().begin();
        it != pShapeNode.children().end();
        ++it)
    {
        vertices[i] = b2Vec2 (it->attribute("x").as_double() / PTM_RATIO,
                              it->attribute("y").as_double() / PTM_RATIO);
        i++;
    }
    shape.CreateChain(vertices, vertCount);
}

b2CircleShape Level::createCircleShapeFromNode(xml_node pShapeNode)
{
    b2CircleShape shape;
    shape.m_radius = pShapeNode.attribute("radius").as_double() / PTM_RATIO;
    return shape;
}

b2PolygonShape Level::createBoxShapeFromNode(xml_node pShapeNode)
{
    b2PolygonShape shape;
    if(pShapeNode.attribute("angle"))
    {
        b2Vec2 center;
        center.Set(pShapeNode.attribute("center_x").as_double()/PTM_RATIO,
                   pShapeNode.attribute("center_y").as_double()/PTM_RATIO);
        shape.SetAsBox(pShapeNode.attribute("width").as_double()/PTM_RATIO,
                       pShapeNode.attribute("height").as_double()/PTM_RATIO,
                       center,
                       Level::toRadians(pShapeNode.attribute("angle").as_double())
                    );
    }
    else
    {
        shape.SetAsBox(pShapeNode.attribute("width").as_double()/PTM_RATIO,
                       pShapeNode.attribute("height").as_double()/PTM_RATIO);
    }
    return shape;
}

void Level::update(float dt)
{
    _world->Step((float32) dt, _velocityIterations, _positionIterations);
    _collisionManager->cleanupDeadBodies();
	bool goalDead = _goal->isDead();
	if (goalDead){
		_obstacleList.erase(std::remove(_obstacleList.begin(), _obstacleList.end(), _goal), _obstacleList.end());
	}
    for(unsigned int i=0; i<_obstacleList.size(); i++)
    {
        _obstacleList[i]->update(dt);
    }
    if(goalDead && this->_finished == false){
        GameoverScreen::win = true;
        GameoverScreen *gameoverScene = GameoverScreen::create();
        this->_finished = true;
        CCDirector::sharedDirector()->replaceScene(CCTransitionFade::create(1.0f, gameoverScene));
    }
}

bool Level::exists(const char *pFileName)
{
    string fullName(pFileName);

    //Tester dans chaque zone de recherche...
    std::vector<string> vec = CCFileUtils::sharedFileUtils()->getSearchPaths();
    for(unsigned i=0;i<vec.size();i++)
    {
        fullName = vec[i] + "level/" + pFileName;
        std::ifstream file(fullName.c_str());
        if(file.good())
        {
            file.close();
            return true;
        }
        else
        {
            file.close();
        }
    }
    return false;
}

string Level::findFullPath(const char *pLevelFileName)
{
    string fullName(pLevelFileName);

    //Tester dans chaque zone de recherche...
    std::vector<string> vec = CCFileUtils::sharedFileUtils()->getSearchPaths();
    for(unsigned i=0;i<vec.size();i++)
    {
        fullName = vec[i] + "level/" + pLevelFileName;
        std::ifstream file(fullName.c_str());
        if(file.good())
        {
            file.close();
            return fullName;
        }
        else
        {
            file.close();
        }
    }
    return string("");
}

b2World* Level::getWorld()
{
    return this->_world;
}

int Level::getMaxPlatforms()
{
    return _maxPlatforms;
}

double Level::toRadians(double pAngle)
{
    return pAngle * M_PI / 180.0;
}

double Level::toDegrees(double pAngle)
{
    return pAngle * 180.0 / M_PI;
}

std::vector<std::string> Level::getAvailableLevelNames()
{
    std::vector<string> availableLevels;
    std::vector<string> searchPaths = CCFileUtils::sharedFileUtils()->getSearchPaths();
    for(int i=0;i<searchPaths.size();i++)
    {
        std::string fullPath(searchPaths[i]);
        fullPath.append("level/");
        DIR *dir;
        struct dirent *ent;
        if ((dir = opendir (fullPath.c_str())) != NULL)
        {
            /* print all the files and directories within directory */
            while ((ent = readdir (dir)) != NULL)
            {

                std::string fileName(ent->d_name);
                int idx = fileName.rfind('.');
                if(idx != std::string::npos)
                {
                    std::string extension = fileName.substr(idx+1);
                    if(extension == "xml")
                    {
                        availableLevels.push_back(fileName);
                    }
                }
            }
        }
        closedir (dir);
    }
    sort(availableLevels.begin(), availableLevels.end());
    return availableLevels;
}

bodyContainer* Level::findBodyByIndex(int pIndex)
{
    int i;
    for(i=0;i<indexedBodyList.size();i++)
    {
        if(indexedBodyList[i].id == pIndex)
        {
            return &indexedBodyList[i];
        }
    }
    CCLog("Body with index %d not found", pIndex);
    return NULL;
}

Level::~Level()
{
    CCLog("Deleting level...");
    while(!_obstacleList.empty())
    {
        Obstacle* ob = _obstacleList.back();
        ob->deleteImage();
        _obstacleList.pop_back();
    }
//    delete _world;
}
