#ifndef HUDLAYER_H
#define HUDLAYER_H

#include "cocos2d.h"


//Class to handle ingame-hud with life or whatever we need in it
class HudLayer : public cocos2d::CCLayer
{
    public:
        HudLayer();
        virtual bool init();
        CREATE_FUNC(HudLayer);

        void setPause(bool pPause);
        void unpause();
        bool isPaused();
        void setScoreLabel(std::string pStr);

private:
        cocos2d::CCMenu* _menu;
        cocos2d::CCMenu* _pauseMenu;
        cocos2d::CCMenuItemImage* _openMenu;
        void onShowMenu(CCObject* pSender);
        void onHideMenu(CCObject* pSender);
        void onReturnButtonPressed(CCObject* pSender);
        void onRestartButtonPressed(CCObject* pSender);
        const float RELOAD_TIME;
        bool _isPaused;
        cocos2d::CCLabelTTF* _scoreLabel;



};

#endif // HUDLAYER_H
