#ifndef PLAYERPLATFORM_H
#define PLAYERPLATFORM_H

#include "cocos2d.h"
#include "Box2D/Box2D.h"
#include "enums.h"
#include "physicalgameobject.h"

#define PTM_RATIO 32

class PlayerPlatform : public PhysicalGameObject
{
    public:
        PlayerPlatform(b2World* pWorld, b2Vec2 pStart, b2Vec2 pEnd, float impulseStrength);
        ~PlayerPlatform();
        std::string getDescription();
        b2Body* _body;
        b2Body* getBody();
        b2Vec2 _start;
        b2Vec2 _end;
        float _impulseStrength;

    private:
};

#endif // PLAYERPLATFORM_H
