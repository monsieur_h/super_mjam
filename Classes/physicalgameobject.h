#ifndef PHYSICALGAMEOBJECT_H
#define PHYSICALGAMEOBJECT_H

#include "cocos2d.h"
#include "enums.h"
#include "Box2D/Box2D.h"

#define PTM_RATIO 32

class PhysicalGameObject : public cocos2d::CCNode
{
protected:

    unsigned int _type;
    b2World* _world;
    b2Body* _body;

public:
    PhysicalGameObject();
    unsigned int getBodyType();
    virtual std::string getDescription();
    ~PhysicalGameObject();
    b2Body* getBody();//Abstract
    void die();
    virtual void onHit(unsigned int pType);
    bool isDead();
    bool _dead;

};

#endif // PHYSICALGAMEOBJECT_H
