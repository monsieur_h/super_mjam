#include "physicalgameobject.h"

USING_NS_CC;

//dtor removes it's body from the world. DO NOT CALL ON COLLISION CALLBACK
PhysicalGameObject::~PhysicalGameObject()
{
}

unsigned int PhysicalGameObject::getBodyType()
{
    return _type;
}

PhysicalGameObject::PhysicalGameObject()
{
    _dead = false;
}

std::string PhysicalGameObject::getDescription()
{
    if((_type & OBSTACLE) != 0)
    {
        return std::string("Type : OBSTACLE");
    }
    else if((_type & PLAYER) != 0)
    {
        return std::string("Type : PLAYER");
    }
    else if((_type & PLATFORM) != 0)
    {
        return std::string("Type : PLATFORM");
    }
    return std::string("");
}

void PhysicalGameObject::die()
{
    _dead = true;
}

bool PhysicalGameObject::isDead()
{
    return _dead;
}

void PhysicalGameObject::onHit(unsigned int pType)
{
    //Abstract
}


b2Body* PhysicalGameObject::getBody()
{
	return this->_body;
}
