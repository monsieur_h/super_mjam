#ifndef PLAYER_H
#define PLAYER_H

#include "Box2D/Box2D.h"
#include "cocos2d.h"
#include "enums.h"
#include <string>
#include "gamescreen.h"
#include "physicalgameobject.h"
#include "gamescreen.h"

class GameScreen;

#define PTM_RATIO 32

class Player : public PhysicalGameObject
{
public:
    Player(b2World* pWorld, b2Vec2 pStartPosition, float linearDamping, float angularDamping, float restitution);
    void update(float dt);
    ~Player();
    std::string getDescription();
    b2Body* getBody();
    void onHitDanger(float pDamage);
    void onHitDanger();
    void onHit(unsigned int pType);
    void setGamescreen(GameScreen* pGs);

private:
    float __timer_t;
    float _breathSpeed;
    float _health;
    float _defaultDamage;
    GameScreen* _gamescreen;
    b2World* _world;
    b2Body* _body;
    b2Fixture* _fixture;
    cocos2d::CCSprite* _bodySprite; //Visual body of the player
    cocos2d::CCSprite* _faceSprite; //TODO: Spine animation : face
};

#endif // PLAYER_H
