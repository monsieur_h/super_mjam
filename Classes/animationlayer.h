#ifndef ANIMATIONLAYER_H
#define ANIMATIONLAYER_H

#include "cocos2d.h"
#include "draftline.h"
#include "playerplatform.h"
#include "level.h"

class Level;

#define PTM_RATIO 32

class AnimationLayer : public cocos2d::CCLayer
{
    public:
        AnimationLayer();
        ~AnimationLayer();
        virtual bool init();
        void initShader();
        virtual void ccTouchesBegan(cocos2d::CCSet* pTouches, cocos2d::CCEvent* pEvent);
        virtual void ccTouchesEnded(cocos2d::CCSet *pTouches, cocos2d::CCEvent *pEvent);
        virtual void ccTouchesMoved(cocos2d::CCSet *pTouches, cocos2d::CCEvent *pEvent);
        virtual void draw();
        void addLevel(Level* pLevel);
        void addEffect(cocos2d::CCNode* node);
        CREATE_FUNC(AnimationLayer);
        bool isFistPlatformPlaced();
        int getPlatformCount();

    private:
        void createPlayerPlatform(cocos2d::CCPoint pP1, cocos2d::CCPoint pP2);
        void drawShapes();
        void drawPolygoneShape(b2Fixture* fixture, b2Body* body, unsigned int pType);
        void drawCircleShape(b2Fixture* fixture, b2Body* body, unsigned int pType);
        void drawChainShape(b2Fixture* fixture, b2Body* body, unsigned int pType);
        void configureOpenGl(unsigned int pType, bool triangleFan);
        DraftLine *_currentLine;
        Level* _level;
        PlayerPlatform* _platform;
        bool _firstPlatformPlaced;
        int _platformCount;

        cocos2d::CCGLProgram* mShaderProgram;
        GLint        mColorLocation;
};

#endif // ANIMATIONLAYER_H
