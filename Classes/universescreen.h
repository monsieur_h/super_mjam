#ifndef UNIVERSESCREEN_H
#define UNIVERSESCREEN_H

#include "cocos2d.h"
#include "gamescreen.h"

class UniverseScreen : public cocos2d::CCScene
{
    public:
        virtual bool init();
        CREATE_FUNC(UniverseScreen);

    private :
        cocos2d::CCMenu* _mainMenu;
        void onWorldClick(cocos2d::CCObject* pSender);
        void onOptionClicked(cocos2d::CCObject* pSender);
		std::vector<cocos2d::CCMenuItemImage*> images;
		std::vector<cocos2d::CCMenuItemFont*> fonts;
		float imgSize;
		int selectedWorld;
};

#endif // UNIVERSESCREEN_H
