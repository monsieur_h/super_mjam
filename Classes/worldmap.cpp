#include "worldmap.h"
#include <math.h>
#include "gamescreen.h"
#include "sharedconfig.h"
#include "optionmenu.h"

USING_NS_CC;

bool WorldMap::init()
{
    CCSize visibleSize = CCDirector::sharedDirector()->getVisibleSize();

    m_layer = CCLayerColor::create(BLACK);
    this->addChild(m_layer);

    m_menu = CCMenu::create();
    this->addChild(m_menu);


    CCMenuItemFont* back = CCMenuItemFont::create(
			"back",
			this,
			menu_selector(WorldMap::onBack));
    back->setFontSizeObj(SharedConfig::get()->menuItemFontSize);
    back->setFontNameObj(FONT_NAME);
    back->setColor(WHITE_3);
	back->setPositionX(-visibleSize.width/2 + back->getContentSize().width/2 + SharedConfig::get()->menuItemMargin);
	back->setPositionY(visibleSize.height/2 - back->getContentSize().height/2 - SharedConfig::get()->menuItemMargin);
	m_menu->addChild(back);

    CCMenuItemImage* menuButton = CCMenuItemImage::create("option.png",
                                                          "option.png",
                                                          this,
                                                          menu_selector(WorldMap::onOptionClicked));
    menuButton->setPositionX(visibleSize.width/2  - menuButton->getContentSize().width + SharedConfig::get()->menuItemMargin/2);
    menuButton->setPositionY(visibleSize.height/2 - menuButton->getContentSize().height - SharedConfig::get()->menuItemMargin/4);
    m_menu->addChild(menuButton);

    CCLog("Worldmap init over");
    return true;
}


void WorldMap::setWorld(World *pWorld)
{
    this->m_world = pWorld;
    initMenuImages();
}

void WorldMap::initMenuImages()
{
    if(m_world == NULL)
        return;

	CCSize visibleSize = CCDirector::sharedDirector()->getVisibleSize();
	vector<string> levelList = this->m_world->getAvailableLevelNames();
	string resourcesPath = this->m_world->getPath();
	int imgSize = 120;
	float rowNb = floor((visibleSize.width-SharedConfig::get()->menuItemMargin*2)/imgSize);
	float imgMargin = ( (visibleSize.width-SharedConfig::get()->menuItemMargin*2) - imgSize*rowNb ) / (rowNb-1);
	float x = - visibleSize.width/2 + SharedConfig::get()->menuItemMargin + imgSize/2;
	float y = visibleSize.height/2 - SharedConfig::get()->menuItemMargin*2 - SharedConfig::get()->menuItemFontSize;

    int lastLevelIndex = SharedConfig::get()->getLastLevelIndex(SharedConfig::get()->getCurrentWorldIndex());
    for(unsigned int i=0; i<levelList.size(); i++)
    {
        string imageName;
        if(i > lastLevelIndex)
        {
            imageName = "level_locked.png";
        }
        else
        {
            imageName = resourcesPath;
            imageName.append("/");
            imageName.append(levelList[i]);
            imageName.append(".png");
        }

        CCMenuItemImage* menuImage = CCMenuItemImage::create(imageName.c_str(),
                                                                imageName.c_str(),
                                                                this,
                                                                menu_selector(WorldMap::onLevelClicked));
		float scale = imgSize / menuImage->getContentSize().width;
		menuImage->setScale(scale);
		menuImage->setPositionX(x);
		menuImage->setPositionY(y - imgSize/2);
		menuImage->setTag(i);
        if(i > lastLevelIndex)
            menuImage->setEnabled(false);
        this->m_menu->addChild(menuImage);
		x += imgSize + imgMargin;
		if ((i+1)%(int)rowNb == 0) {
			x = - visibleSize.width/2 + SharedConfig::get()->menuItemMargin + imgSize/2;
			y -= imgMargin + imgSize;
		}
    }
}

void WorldMap::onLevelClicked(CCMenuItemImage* pSender)
{
    int levelIndex = pSender->getTag();
    SharedConfig::get()->onEnterLevel(levelIndex);

    vector<string> list = this->m_world->getAvailableLevelNames();
    string levelName = list[levelIndex];
    levelName.append(".xml");

    string worldPath(this->m_world->getPath());
    worldPath.append("/");

    levelName.insert(0, worldPath);

    GameScreen::setLevelFile(levelName);
    GameScreen* gamescene = GameScreen::create();
    CCDirector::sharedDirector()->replaceScene(CCTransitionFade::create(1.0f, gamescene));
}

void WorldMap::onBack(CCObject* pSender)
{
    UniverseScreen *universeScene = UniverseScreen::create();
    CCDirector::sharedDirector()->replaceScene(CCTransitionFade::create(1.0f, universeScene));
}

void WorldMap::onOptionClicked(CCObject *pSender)
{
    OptionMenu* optionscene = OptionMenu::create();
    CCDirector::sharedDirector()->replaceScene(CCTransitionFade::create(1.0f, optionscene));
}
