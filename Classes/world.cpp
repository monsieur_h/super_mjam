#include "world.h"
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fstream>
#include <algorithm>

USING_NS_CC;

World::World(string pName) : m_name(pName)
{
    this->setAvailableLevelNames();
}

void World::setAvailableLevelNames()
{
    vector<string> searchPaths = CCFileUtils::sharedFileUtils()->getSearchPaths();
    for(int i=0;i<searchPaths.size();i++)
    {
        string fullPath(searchPaths[i]);
        fullPath.append("level/");
        fullPath.append(this->m_name);
        DIR *dir;
        struct dirent *ent;
        if ((dir = opendir (fullPath.c_str())) != NULL)
        {
            this->m_fullPath = fullPath;
            /* print all the files and directories within directory */
            while ((ent = readdir (dir)) != NULL)
            {
                string fileName(ent->d_name);
                int idx = fileName.rfind('.');
                if(idx != string::npos)
                {
                    string extension = fileName.substr(idx+1);
                    if(extension == "xml")
                    {
                        string basename = fileName.substr(0, idx);
                        m_levelNames.push_back(basename);
                    }
                }
            }
        }
        closedir (dir);
    }
    std::sort(m_levelNames.begin(), m_levelNames.end());
}

vector<string> World::getAvailableLevelNames()
{
    return this->m_levelNames;
}

string World::getPath()
{
    return this->m_fullPath;
}

vector<string> World::getAvailableWorldNames()
{
    vector<string> worldList;
    vector<string> searchPaths = CCFileUtils::sharedFileUtils()->getSearchPaths();
    for(int i=0;i<searchPaths.size();i++)
    {
        string fullPath(searchPaths[i]);
        fullPath.append("level/");
        DIR *dir;
        struct dirent *ent;
        if ((dir = opendir (fullPath.c_str())) != NULL)
        {
            while ((ent = readdir (dir)) != NULL)
            {
                if(ent->d_type & DT_DIR)
                {
                    if((strcmp (ent->d_name, "..") != 0 &&
                        strcmp (ent->d_name, ".") != 0))
                    {
                        string s(ent->d_name);
                        worldList.push_back(s);
                    }
                }
            }
        }
        closedir (dir);
    }
    std::sort(worldList.begin(), worldList.end());
    return worldList;
}

string World::getName()
{
    return this->m_name;
}
