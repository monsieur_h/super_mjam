#include "gamescreen.h"
#include "gameoverscreen.h"
#include "sharedconfig.h"

USING_NS_CC;

static string _levelName("");

bool GameScreen::init()
{
    //TODO:load dynamically N layers from a level
    _backgroundLayer = CCLayerColor::create(BLACK);
    _animationLayer  = AnimationLayer::create();
    _hudLayer = HudLayer::create();

    this->addChild(_backgroundLayer, 0);
    this->addChild(_animationLayer, 1);
    this->addChild(_hudLayer, 10);

    _screenBorderRatio = 4.0f/5.0f;
    _cameraTrackingSpeed = 5.2f;

    loadLevel(_levelName);

    //REMOVE?
    CCSize visibleSize = CCDirector::sharedDirector()->getVisibleSize();
    CCPoint halfScreen(visibleSize.width/2, visibleSize.height/2);

    _animationLayer->setPosition(halfScreen);
    _animationLayer->addLevel(_currentLevel);
    //_backgroundLayer->setPosition(halfScreen);

    b2Vec2 playerPos = b2Vec2(0, 0);
	
    _player = new Player(_currentLevel->getWorld(), playerPos, _currentLevel->linearDamping, _currentLevel->angularDamping,  _currentLevel->restitution);	
    _animationLayer->addChild(_player);
    _player->setGamescreen(this);

    _elapsedTime = 0;
	
    scheduleUpdate();
    return true;
}

bool GameScreen::loadLevel(string pLevelName)
{
    _levelName = pLevelName;
    _currentLevel = new Level(pLevelName, _animationLayer);
    return true;
}

GameScreen::~GameScreen()
{
    CCLog("Deleting gamescreen...");
    delete _currentLevel;
}

void GameScreen::update(float dt)
{
    if(this->_hudLayer->isPaused())
    {
        _animationLayer->setTouchEnabled(false);
        return;
    }
    else
    {
        _animationLayer->setTouchEnabled(true);
    }

    if(!this->_animationLayer->isFistPlatformPlaced())
        return;

    if(this->_player->isDead())
    {
        GameoverScreen::win = false;
        GameoverScreen *gameoverScene = GameoverScreen::create();
        CCDirector::sharedDirector()->replaceScene(CCTransitionFade::create(1.0f, gameoverScene));
    }

    if(_currentLevel != NULL)
    {
        _currentLevel->update(dt);
    }

    if(_player != NULL)
    {
        moveCamera(dt);
    }
    if(!this->_hudLayer->isPaused()
            && !this->_player->isDead()
            && this->_animationLayer->isFistPlatformPlaced())
        _elapsedTime += dt;
    std::stringstream s;
    s << "Time :\t" << std::floor(this->_elapsedTime) << "\tPlatforms :\t" << this->_animationLayer->getPlatformCount() << std::endl;
    string std = s.str();
    this->_hudLayer->setScoreLabel(std);
}

void GameScreen::setLevelFile(string levelName)
{
    _levelName = levelName;
}

bool GameScreen::isLastLevel()
{
    std::vector<string> list = Level::getAvailableLevelNames();
    if(list[list.size()-1] == _levelName)
    {
        return true;
    }
    else
    {
        return false;
    }
}

void GameScreen::nextLevel()
{
    CCLog("Finding next level" );
    string nextLevel;
    World* destinationWorld;
    int levelIndex;
    if(SharedConfig::get()->isLastLevel())
    {
        destinationWorld = SharedConfig::get()->getNextWorld();
        levelIndex = 0;
    }
    else
    {
        levelIndex = SharedConfig::get()->getCurrentLevel() + 1;
        destinationWorld = SharedConfig::get()->getCurrentWorld();
    }
    nextLevel = destinationWorld->getPath();

    std::vector<string> list = destinationWorld->getAvailableLevelNames();
    nextLevel.append("/");
    nextLevel.append(list[levelIndex]);
    nextLevel.append(".xml");
    _levelName = nextLevel;
    SharedConfig::get()->onEnterLevel(levelIndex);
}

void GameScreen::moveCamera(float dt)
{
    CCPoint playerPos = _player->getPosition();
    playerPos = CCDirector::sharedDirector()->convertToUI(playerPos);
    playerPos.x *= -1;
    CCSize visibleSize = CCDirector::sharedDirector()->getVisibleSize();
    CCPoint halfScreen(visibleSize.width/2, -visibleSize.height/2);
    CCPoint distance = playerPos + halfScreen - _animationLayer->getPosition();

    if(distance.getLength() > 0)
    {
        CCPoint direction= distance.normalize();
        float speed = _cameraTrackingSpeed * distance.getLength();
            _animationLayer->setPosition(_animationLayer->getPositionX() + direction.x *  speed * dt,
                                         _animationLayer->getPositionY() + direction.y *  speed * dt);
    }
}


void GameScreen::setPause(bool pPause)
{
    this->_hudLayer->setPause(pPause);
}

void GameScreen::draw()
{

}

void GameScreen::addEffect(cocos2d::CCActionInterval* pEffect)
{
    this->runAction(CCSequence::create(pEffect, pEffect->reverse(), CCStopGrid::create(), NULL));
}
