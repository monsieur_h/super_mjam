#include "splashscreen.h"
#include "sharedconfig.h"
#include "SimpleAudioEngine.h"

USING_NS_CC;

CCScene* SplashScreen::scene()
{
    CCScene *scene = CCScene::create();
    SplashScreen *layer = SplashScreen::create();
    layer->setTouchEnabled(true);
    scene->addChild(layer);

    return scene;
}

bool SplashScreen::init()
{
    CocosDenshion::SimpleAudioEngine::sharedEngine()->playBackgroundMusic("sound/background.wav", true);
    if(!CCLayerColor::initWithColor(BLACK))
    {
        return false;
    }
    this->setTouchEnabled(true);
    this->_autoCloseTime = 5;
    this->_fadingTime = 1;

    CCSize visibleSize = CCDirector::sharedDirector()->getVisibleSize();

    CCSprite* img = CCSprite::create("splashscreen.png");

    //Scale to fit screen
    float scaleX = visibleSize.width / img->getContentSize().width;
    float scaleY = visibleSize.height / img->getContentSize().height;
    float scale = (scaleY < scaleX) ? scaleY : scaleX;
    img->setScale(scale);

    //Position at center
    img->setAnchorPoint(ccp(0.5, 0.5));
    img->setPosition(visibleSize/2);
    this->addChild(img);

    CCDelayTime * delayAction = CCDelayTime::create(_autoCloseTime);
    CCCallFunc * callFunc = CCCallFunc::create(this, callfunc_selector(SplashScreen::close));
    this -> runAction( CCSequence::createWithTwoActions(delayAction, callFunc));

    return true;
}

void SplashScreen::close()
{
    UniverseScreen *universeScene = UniverseScreen::create();
    CCDirector::sharedDirector()->replaceScene(CCTransitionFade::create(_fadingTime, universeScene));
}

void SplashScreen::ccTouchEnded(cocos2d::CCTouch *pTouch, cocos2d::CCEvent *pEvent)
{
    this->close();
}

bool SplashScreen::ccTouchBegan(cocos2d::CCTouch *pTouch, cocos2d::CCEvent *pEvent){return true;}

void SplashScreen::registerWithTouchDispatcher()
{
    CCDirector::sharedDirector()->getTouchDispatcher()->addTargetedDelegate(this, cocos2d::kCCMenuHandlerPriority - 1, true);
}
