#ifndef GAMEOVERSCREEN_H
#define GAMEOVERSCREEN_H

#include "cocos2d.h"
#include "gamescreen.h"
#include "menuscreen.h"

class GameoverScreen : public cocos2d::CCScene
{
    public:
        virtual bool init();
        CREATE_FUNC(GameoverScreen);
        static bool win;

    private :
        cocos2d::CCMenu* _mainMenu;
        void onPlayAgain(cocos2d::CCObject* pSender);
        void onMenu(CCObject* pSender);
        void onNextLevel(CCObject* pSender);
};

#endif // GAMEOVERSCREEN_H
