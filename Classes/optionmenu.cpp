#include "optionmenu.h"
#include "sharedconfig.h"
#include "universescreen.h"

USING_NS_CC;

bool OptionMenu::init()
{
    const float FONT_SIZE = SharedConfig::get()->menuItemFontSize;
    const float MARGIN = SharedConfig::get()->menuItemMargin;
    CCSize visibleSize = CCDirector::sharedDirector()->getVisibleSize();

    m_layer = CCLayerColor::create(BLACK);
    this->addChild(m_layer);

    m_menu = CCMenu::create();
    this->addChild(m_menu);

    CCMenuItemFont* resumeButton = CCMenuItemFont::create(
                "back",
                this,
                menu_selector(OptionMenu::onResumeClicked));

    CCMenuItemFont* eraseButton = CCMenuItemFont::create(
                "erase saved games",
                this,
                menu_selector(OptionMenu::onEraseClicked));

    CCMenuItemFont* exitButton = CCMenuItemFont::create(
                "quit game",
                this,
                menu_selector(OptionMenu::onExitClicked));


    std::vector<CCMenuItemFont*> items;
    items.push_back(resumeButton);
    items.push_back(eraseButton);
    items.push_back(exitButton);

    float yPos= FONT_SIZE * (items.size()-1) + MARGIN * (items.size()-1);
    yPos /= 2;

    for(unsigned int i=0; i<items.size(); i++)
    {
        items[i]->setFontSizeObj(FONT_SIZE);
        items[i]->setFontNameObj(FONT_NAME);
        items[i]->setColor(WHITE_3);
        items[i]->setPositionY(yPos);
        m_menu->addChild(items[i]);
        yPos -= items[i]->fontSizeObj() + MARGIN;
    }
    return true;
}

void OptionMenu::onResumeClicked(CCObject *pSender)
{
    UniverseScreen *universeScene = UniverseScreen::create();
    CCDirector::sharedDirector()->replaceScene(CCTransitionFade::create(0.3f, universeScene));
}

void OptionMenu::onEraseClicked(CCObject *pSender)
{
    SharedConfig::get()->resetSave();
}

void OptionMenu::onExitClicked(CCObject *pSender)
{
    //TODO:desactiver
    exit(EXIT_SUCCESS);
}
