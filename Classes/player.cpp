#include "player.h"

USING_NS_CC;

Player::Player(b2World* pWorld, b2Vec2 pStartPosition, float linearDamping,  float angularDamping, float restitution) :
    _world(pWorld),
    _health(100),
    _defaultDamage(100)
{
    _type = PLAYER;

    //Physics
    b2BodyDef bd;
    bd.type = b2_dynamicBody;
	//HABIB
	bd.linearDamping = linearDamping;
	bd.angularDamping = angularDamping;
    bd.position.Set(pStartPosition.x, pStartPosition.y);
    bd.fixedRotation = false;


    b2CircleShape circle;
    circle.m_radius = 30.0f / PTM_RATIO;

    _body = _world->CreateBody(&bd);

    b2FixtureDef fd;
    fd.shape = &circle;
    fd.density = 1.0;
    fd.friction = 0.9f;
    fd.restitution = restitution;
	fd.filter.categoryBits = PLAYER;
	fd.filter.maskBits = PLAYER | PLATFORM | OBSTACLE | GOAL;
    _fixture = _body->CreateFixture(&fd);

    _fixture->SetUserData(this);
    _body->SetUserData(this);

    //Images
    //_bodySprite = CCSprite::create("avatar_body.png");
    //_bodySprite->setScale(0.15f);
    //addChild(_bodySprite);

    this->_gamescreen = NULL;

    //Update
    scheduleUpdate();
}

void Player::update(float dt)
{
    setPosition(_body->GetPosition().x * PTM_RATIO,
                _body->GetPosition().y * PTM_RATIO);
    setRotation(-1 * CC_RADIANS_TO_DEGREES(_body->GetAngle()));
}

Player::~Player()
{
}

std::string Player::getDescription()
{
    return std::string("Type : PLAYER");
}

b2Body* Player::getBody()
{
    return _body;
}

void Player::onHitDanger()
{
    this->onHitDanger(this->_defaultDamage);
}

void Player::onHitDanger(float pDamage)
{
    this->_health -= pDamage;
    if(this->_health <= 0)
    {
        this->die();
    }
}



void Player::onHit(unsigned int pType)
{
    if((pType & DANGEROUS) != 0)
    {
        this->onHitDanger();
    }
    if((pType & CONTACT_IMPULSE))
    {
        CCShaky3D* effect = CCShaky3D::create(0.25f, CCSizeMake(5,5), 5, false);
        this->_gamescreen->addEffect((CCActionInterval* )effect);
    }
}

void Player::setGamescreen(GameScreen *pGs)
{
    CCLog("Setting gamescreen");
    this->_gamescreen = pGs;
}
