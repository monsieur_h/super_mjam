#ifndef SPLASHSCREEN_H
#define SPLASHSCREEN_H

#include "cocos2d.h"
#include "universescreen.h"

class SplashScreen : public cocos2d::CCLayerColor
{
    public:
        virtual bool init();
        static cocos2d::CCScene* scene();
        virtual void ccTouchEnded(cocos2d::CCTouch *pTouch, cocos2d::CCEvent *pEvent);
        virtual bool ccTouchBegan(cocos2d::CCTouch *pTouch, cocos2d::CCEvent *pEvent);
        void registerWithTouchDispatcher();
        CREATE_FUNC(SplashScreen);

    private:
        void close(CCObject* pSender);
        void close();
        float _autoCloseTime;
        float _fadingTime;
};

#endif // SPLASHSCREEN_H
