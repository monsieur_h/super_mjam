#include "collisionmanager.h"

USING_NS_CC;

CollisionManager::CollisionManager(b2World* pWorld) : _world(pWorld)
{
}

void CollisionManager::EndContact(b2Contact *pContact)
{

}

void CollisionManager::BeginContact(b2Contact *pContact)
{
    PhysicalGameObject* objA = (PhysicalGameObject*) pContact->GetFixtureA()->GetUserData();
    PhysicalGameObject* objB = (PhysicalGameObject*) pContact->GetFixtureB()->GetUserData();
    unsigned int tagA = objA->getBodyType();
    unsigned int tagB = objB->getBodyType();
    unsigned int collisionTag = tagA | tagB;

    if((collisionTag & PLAYER) !=0 )
    {
//        CCLog("PLAYER" );
        Player* p;
        unsigned int tag;
        if((tagA & PLAYER) != 0 )
        {
            p = (Player*) pContact->GetFixtureA()->GetUserData();
            tag = tagB;
        }
        else//PLAYER == B
        {
            p = (Player*) pContact->GetFixtureB()->GetUserData();
            tag = tagA;
        }
        p->onHit(tag);
    }


    if((collisionTag & PLAYER) != 0
            && (collisionTag & PLATFORM) != 0)//Colision PLAYER/PLATFORM
    {
        if((tagA & PLATFORM) != 0)//PLATFORM is A
        {
            safelyKill(objA);
            PlayerPlatform* platform = (PlayerPlatform*) pContact->GetFixtureA()->GetUserData();
            Player* player = (Player*) pContact->GetFixtureB()->GetUserData();
            applyImpulse(player->getBody(), platform->_start, platform->_end, platform->_impulseStrength);
        }
        else//PLATFORM is B
        {
            safelyKill(objB);
            PlayerPlatform* platform = (PlayerPlatform*) pContact->GetFixtureB()->GetUserData();
            Player* player = (Player*) pContact->GetFixtureA()->GetUserData();
            applyImpulse(player->getBody(), platform->_start, platform->_end, platform->_impulseStrength);
        }
    }
	if((collisionTag & CONTACT_IMPULSE) != 0)//Colision PLAYER/CONTACT_IMPULSE
    {
        if((tagA & CONTACT_IMPULSE) != 0)//CONTACT_IMPULSE is A
        {
			PhysicalGameObject* obstacle = (PhysicalGameObject*) pContact->GetFixtureA()->GetUserData();
			if ((tagB & PLAYER) != 0){
				Player* player = (Player*) pContact->GetFixtureB()->GetUserData();
				applyImpulse(player->getBody(), obstacle->getBody());
			}
			else {
				PhysicalGameObject* player = (PhysicalGameObject*) pContact->GetFixtureB()->GetUserData();
				applyImpulse(player->getBody(), obstacle->getBody());
			}
        }
        else//CONTACT_IMPULSE is B
        {
            PhysicalGameObject* obstacle = (PhysicalGameObject*) pContact->GetFixtureB()->GetUserData();
            if ((tagB & PLAYER) != 0){
				Player* player = (Player*) pContact->GetFixtureA()->GetUserData();
				applyImpulse(player->getBody(), obstacle->getBody());
			}
			else {
				PhysicalGameObject* player = (PhysicalGameObject*) pContact->GetFixtureA()->GetUserData();
				applyImpulse(player->getBody(), obstacle->getBody());
			}
        }
    }

	if((collisionTag & CONTACT_DISSAPEAR) != 0)//Colision PLAYER/CONTACT_DISSAPEAR
    {
        if((tagA & CONTACT_DISSAPEAR) != 0)//CONTACT_DISSAPEAR is A
        {
            safelyKill(objA);
        }
        else//CONTACT_DISSAPEAR is B
        {
            safelyKill(objB);
        }
    }

    if(((collisionTag & PLAYER) != 0)//atteindre la fin du niveau (collision PLAYER/GOAL)
        && ((collisionTag & GOAL) != 0))
    {
        if((tagA & GOAL) != 0)//GOAL is A
        {
            safelyKill(objA);
        }
        else
        {
            safelyKill(objB);
        }
    }
    if(((collisionTag & DANGEROUS) != 0)
            && (collisionTag & PLAYER) != 0)
    {
        CCLog("DANGEROUS/PLAYER");
        if((tagA & DANGEROUS) != 0)//DANGEROUS OBSTACLE is A
        {
            Player* player = (Player*) pContact->GetFixtureB()->GetUserData();
            player->onHit(tagA);
        }
        else
        {
            Player* player = (Player*) pContact->GetFixtureA()->GetUserData();
            player->onHit(tagB);
        }
    }
    if(((collisionTag & CONTACT_DYNAMIC) != 0))
            //&& (collisionTag & PLAYER) != 0) //Collision with an OBSTACLE
    {
        if ((tagB & CONTACT_DYNAMIC) != 0)//OBSTACLE is B
		{
			Obstacle* obstacle = (Obstacle*) pContact->GetFixtureB()->GetUserData();
			obstacle->activate();
		}
        else//OBSTACLE is A
		{			
			Obstacle* obstacle = (Obstacle*) pContact->GetFixtureA()->GetUserData();
			obstacle->activate();
		}
    }
	if(((collisionTag & CONTACT_REVERSE) != 0))
            //&& (collisionTag & PLAYER) != 0) //Collision with an OBSTACLE
    {
        if ((tagB & CONTACT_DYNAMIC) != 0)//OBSTACLE is B
		{
			Obstacle* obstacle = (Obstacle*) pContact->GetFixtureB()->GetUserData();
			obstacle->activateReverse();
		}
        else//OBSTACLE is A
		{			
			Obstacle* obstacle = (Obstacle*) pContact->GetFixtureA()->GetUserData();
			obstacle->activateReverse();
		}
    }
}

void CollisionManager::cleanupDeadBodies()
{
    b2Body* node = _world->GetBodyList();
    while(node != NULL)
    {
        b2Body* body = node;
        node = node->GetNext();
        PhysicalGameObject* obj = (PhysicalGameObject*) body->GetUserData();
        if(obj != NULL && obj->isDead())
        {
            _world->DestroyBody(body);
        }
    }
}

void CollisionManager::applyImpulse(b2Body *pBody, b2Vec2 pStart, b2Vec2 pEnd, float pStren)
{
    b2Vec2 platform = pEnd - pStart;
    b2Vec2 normal   = b2Cross(-1, platform);
    normal.Normalize();
    b2Vec2 impulsion =  pStren * normal;


    //Resetting velocity
    b2Vec2 ZERO(0.0f, 0.0f);
    pBody->SetLinearVelocity(ZERO);
    pBody->SetAngularVelocity(0.0f);

    pBody->ApplyLinearImpulse(impulsion, pBody->GetPosition());
}

void CollisionManager::applyImpulse(b2Body *pBody, b2Body* body)
{
	b2Vec2 impulsion;
	b2Fixture *fixture = body->GetFixtureList();
	b2Shape* shape = fixture->GetShape();
	if (shape->GetType() == b2Shape::e_circle)
	{
		b2Vec2 normal = pBody->GetPosition() - body->GetPosition();
		normal.Normalize();
		impulsion =  60 * normal;
	}
	else if (shape->GetType() == b2Shape::e_polygon)
	{
		b2Vec2 pStart = b2Vec2_zero, pEnd = b2Vec2_zero;
		float dist1 = 0, dist2 = 0;
		b2Fixture *fixture = body->GetFixtureList();
		b2PolygonShape* pShape = (b2PolygonShape*) fixture->GetShape();
        int vertexCount = pShape->GetVertexCount();
		b2Vec2 normal;
		if (vertexCount == 4) {
			b2Vec2* verts = new b2Vec2[3];
			for(int i = 0 ; i < 3 ; i++ )
			{
				verts[i] = pShape->GetVertex(i);
				verts[i] = body->GetWorldPoint( verts[i] );
			}

			if (verts[0].x == verts[1].x) {
				verts[0] = verts[1];
				verts[1] = verts[2];
			}

			if (verts[1].x < verts[0].x) {
				b2Vec2 vertTemp = verts[0];
				verts[0] = verts[1];
				verts[1] = vertTemp;
			}

			if (verts[0].x < pBody->GetPosition().x && verts[1].x > pBody->GetPosition().x){
				if (verts[0].y < pBody->GetPosition().y){
					//cas ou la balle doit monter verticalement
					normal = b2Vec2(0,1);
				}
				else {
					//cas ou la balle doit decendre verticalement
					normal = b2Vec2(0,-1);
				}
			}
			else {
				if (verts[0].x > pBody->GetPosition().x){
					//cas ou la balle doit aller � gauche
					normal = b2Vec2(-1,0);
				}
				else {
					//cas ou la balle doit aller � droite
					normal = b2Vec2(1,0);
				}
			}
		}
		else {
			b2Vec2 normal = pBody->GetPosition() - body->GetPosition();
			normal.Normalize();
		}
		impulsion =  60 * normal;
	}

		

	//Resetting velocity
	b2Vec2 ZERO(0.0f, 0.0f);
	pBody->SetLinearVelocity(ZERO);

	pBody->SetAngularVelocity(0.0f);

	pBody->ApplyLinearImpulse(impulsion, pBody->GetPosition());
}

void CollisionManager::safelyKill(PhysicalGameObject *pObject)
{
    Obstacle* obstaclePtr = dynamic_cast<Obstacle*>(pObject);

    if(obstaclePtr != NULL)//Si le cast est OK, le detruire comme Obstacle
    {
        obstaclePtr->die();
    }
    else//Sinon le detruire comme objet
    {
        pObject->die();
    }
}
